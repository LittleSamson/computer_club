############################################################
# A Room class!
class Room(object):
    def __init__(self, num, options, description):
        # Save off the inputs:
        # num        : the number of this room
        # description: a description of the room to be printed upon entry
        # options    : a dict mapping directions (N, S, E, W) to room numbers
        self.num         = num
        self.options     = options
        self.description = description

    def enter(self,):
        # Enter the room and describe it
        print(self.description)
        
        # Tell the player the options and get her choice
        response = None
        while response not in self.options:
            exits = list(self.options)
            exits.sort()
            print('Obvious exits are', exits)
            print("Which way would you like to go?")
            response = input()

        # Report which way she went
        directions = {'N': 'north', 'S': 'south', 'E': 'east', 'W': 'west'}
        print('You went %s.' % directions[response])
        print()

        # Return the room number of the response
        return self.options[response]

############################################################
# A Trap is special kind of room with no choices: you just die.
class Trap(Room):
    def enter(self,):
        print(self.description)
        print('Game Over.')

############################################################
# An Exit is a special kind of room with no choices: you win!
class Exit(Room):
    def enter(self,):
        print(self.description)
        print('Congratulations!')
        return None

############################################################
# A dungeon class!
class Dungeon(object):
    def __init__(self,):
        # The name and author of the game
        self.title = 'Escape the dungon'
        self.author = 'David Hansen'

        # Make a list of rooms
        self.rooms = []

        # add each room to the list
        # It's up to you to make sure that your dungeon is complete!
        # self.add_room()
        # self.add_trap()
        # etc.

    # This helper function adds a regular Room to the list of rooms
    def add_room(self, num, options, description):
        self.rooms.append(Room(num, options, description))

    # This helper function adds a Trap to the list of rooms
    # No options are necessary since there is no escape!
    def add_trap(self, num, description):
        self.rooms.append(Trap(num, [], description))

    # This helper function adds an Exit to the list of rooms
    # No options are necessary since you win!
    def add_exit(self, num, description):
        self.rooms.append(Exit(num, [], description))

    # This helper function adds a regular Room to the list of rooms
    # but marks its number as the room in which the game starts
    def add_entrance(self, num, options, description):
        self.add_room(num, options, description)
        self.entrance_num = num

    # This function controls the gameplay
    def explore(self,):
        # Print some text to start the game
        print()
        print("    %s" % self.title)
        print("        by %s" % self.author)
        print()
        print("Press Enter to start exploring.")
        input()
        
        # Start at the entrance
        room_number = self.entrance_num

        # Keep going through rooms until you get to the exit or a trap.
        while room_number is not None:
#            # Clear the screen for the next room
#            print('\n' * 100)

            # Find the current room in the list using the latest room number
            current_room = None
            for room in self.rooms:
                if room.num == room_number:
                    current_room = room

            # enter this room and the number of the player's next choice
            room_number = current_room.enter()
        

############################################################
# Game play!

# Make a dungeon
dungeon = Dungeon()

# Explore it.
dungeon.explore()
