####################################################################################################
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

import random
import shapes
import sys

AIR = 0
STONE = 4
GLOWSTONE = 89
PANE = 95
WOOL = 35

COLORS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

####################################################################################################
def build_x_arch(x, y, z, width=5, height=16, block=PANE, attr=3):
    """width should be an odd number"""
    wd2 = (width - 1) // 2
    tri_height = wd2
    rec_height = height - tri_height
    mc.setBlocks(x, y, z - wd2, x, y + rec_height - 1, z + wd2, block, attr)

    for cury in range(y + rec_height, y + height):
        wd2 -= 1
        mc.setBlocks(x, cury, z - wd2, x, cury, z + wd2, block, attr)
        
####################################################################################################
def build_z_arch(x, y, z, width=5, height=16, block=PANE, attr=3):
    """width should be an odd number"""
    wd2 = (width - 1) // 2
    tri_height = wd2
    rec_height = height - tri_height
    mc.setBlocks(x - wd2, y, z, x + wd2, y + rec_height - 1, z, block, attr)
    for cury in range(y + rec_height, y + height):
        wd2 -= 1
        mc.setBlocks(x - wd2, cury, z, x + wd2, cury, z, block, attr)
        
####################################################################################################
def build_tower_floor(x, y, z, block, window, width=18, height=18):
    west = (x, y, z - width / 2)
    east = (x, y, z + width / 2)
    south  = (x - width / 2, y, z)
    north  = (x + width / 2, y, z)
    sw = (x - width / 2, y, z - width / 2)
    nw = (x + width / 2, y, z - width / 2)
    se = (x - width / 2, y, z + width / 2)
    ne = (x + width / 2, y, z + width / 2)

    # floor
    mc.setBlocks(sw[0], sw[1], sw[2], ne[0], ne[1], ne[2], block)

    # walls 
    mc.setBlocks(sw[0] + 1, sw[1], sw[2], se[0] + 1, se[1] + height, se[2], block) # south
    mc.setBlocks(nw[0] - 1, nw[1], nw[2], ne[0] - 1, ne[1] + height, ne[2], block) # north
    mc.setBlocks(sw[0], sw[1], sw[2] + 1, nw[0], nw[1] + height, nw[2] + 1, block) # west
    mc.setBlocks(se[0], se[1], se[2] - 1, ne[0], ne[1] + height, ne[2] - 1, block) # east

    # corners where walls meet
    mc.setBlocks(sw[0], sw[1], sw[2], sw[0] + 1, sw[1] + height, sw[2] + 1, block)
    mc.setBlocks(se[0], se[1], se[2] - 1, se[0] + 1, se[1] + height, se[2], block)
    mc.setBlocks(nw[0] - 1, nw[1], nw[2], nw[0], nw[1] + height, nw[2] + 1, block)
    mc.setBlocks(ne[0] - 1, ne[1], ne[2] - 1, ne[0], ne[1] + height, ne[2], block)

    # arches
    build_x_arch(sw[0] + 1, sw[1] + 2, sw[2] + 5, block=window) # south wall
    build_x_arch(se[0] + 1, se[1] + 2, se[2] - 5, block=window) # south wall
    build_x_arch(nw[0] - 1, nw[1] + 2, nw[2] + 5, block=window) # north wall
    build_x_arch(ne[0] - 1, ne[1] + 2, ne[2] - 5, block=window) # north wal

    build_z_arch(sw[0] + 5, sw[1] + 2, sw[2] + 1, block=window) # west wall
    build_z_arch(nw[0] - 5, nw[1] + 2, nw[2] + 1, block=window) # west wall
    build_z_arch(se[0] + 5, se[1] + 2, se[2] - 1, block=window) # east wall
    build_z_arch(ne[0] - 5, ne[1] + 2, ne[2] - 1, block=window) # east wall

    # roof
#    mc.setBlocks(sw[0] + 1, sw[1] + height, sw[2] + 1, ne[0] - 1, ne[1] + height + 1, ne[2] - 1, block)

####################################################################################################
def build_tower(x, y, z, block, window, floors=3, floor_height=20):
    width = 18
    height = 64
    west = (x, y, z - width / 2)
    east = (x, y, z + width / 2)
    south  = (x - width / 2, y, z)
    north  = (x + width / 2, y, z)
    sw = (x - width / 2, y, z - width / 2)
    nw = (x + width / 2, y, z - width / 2)
    se = (x - width / 2, y, z + width / 2)
    ne = (x + width / 2, y, z + width / 2)

    # build each floor of the tower
    for floor in range(floors):
        build_tower_floor(x, y + floor_height * floor, z, block, window, width=width, height=floor_height)

    # roof
    roof = y + floor_height * floors
    mc.setBlocks(sw[0], roof, sw[2], ne[0], roof, ne[2], block)
    mc.setBlocks(sw[0], roof + 1, sw[2], se[0], roof + 1, se[2], block)
    mc.setBlocks(nw[0], roof + 1, nw[2], ne[0], roof + 1, ne[2], block)
    mc.setBlocks(sw[0], roof + 1, sw[2], nw[0], roof + 1, nw[2], block)
    mc.setBlocks(se[0], roof + 1, se[2], ne[0], roof + 1, ne[2], block)
    for index in range(0, width + 1, 2):
        mc.setBlocks(sw[0], roof + 2, sw[2] + index, sw[0], roof + 3, sw[2] + index, block)
        mc.setBlocks(nw[0], roof + 2, nw[2] + index, nw[0], roof + 3, nw[2] + index, block)
        mc.setBlocks(sw[0] + index, roof + 2, sw[2], sw[0] + index, roof + 3, sw[2], block)
        mc.setBlocks(se[0] + index, roof + 2, se[2], se[0] + index, roof + 3, se[2], block)

    # cones
    shapes.build_cone(sw[0] + 2, roof + 19, sw[2] + 2, 20, -1 / 20, block)
    shapes.build_cone(se[0] + 2, roof + 19, se[2] - 2, 20, -1 / 20, block)
    shapes.build_cone(nw[0] - 2, roof + 19, nw[2] + 2, 20, -1 / 20, block)
    shapes.build_cone(ne[0] - 2, roof + 19, ne[2] - 2, 20, -1 / 20, block)

####################################################################################################
def build_x_wall(x, y, z, width, height, block, window, section_width=9):
    mc.setBlocks(x, y, z, x, y + height, z + width, block)

    section_count = width // section_width
    for section in range(section_count):
        mc.setBlocks(x - 1, y, z + section * section_width, x + 1, y + height, z + section * section_width + 1, block)
        build_x_arch(x, y + 2, z + section * section_width + 5, height=(height - 4), block=window)
    mc.setBlocks(x - 1, y, z + width - 1, x + 1, y + height, z + width, block)

####################################################################################################
def build_z_wall(x, y, z, width, height, block, window, section_width=9):
    """width mod section_width should be 1"""
    mc.setBlocks(x, y, z, x + width, y + height, z, block)

    section_count = width // section_width
    for section in range(section_count):
        mc.setBlocks(x + section * section_width, y, z - 1, x + section * section_width + 1, y + height, z + 1, block)
        build_z_arch(x + section * section_width + 5, y + 2, z, height=(height - 4), block=window)
    mc.setBlocks(x + width - 1, y, z - 1, x + width, y + height, z + 1, block)

####################################################################################################
def build_cathedral(xx, yy, zz, block, window, roof):
    # towers
    build_tower(xx + 20, yy + 0, zz - 20, block, window)
    build_tower(xx + 20, yy + 0, zz + 20, block, window)

    # side walls
    build_z_wall(xx + 29, yy + 0, zz - 20, 82, 40, block, window)
    build_z_wall(xx + 29, yy + 0, zz + 20, 82, 40, block, window)

    # roof
    for z in range(20):
        mc.setBlocks(xx + 28, yy + 60 - z, zz - z, xx + 111, yy + 60 - z, zz - z, roof, 13)
        mc.setBlocks(xx + 28, yy + 60 - z, zz + z, xx + 111, yy + 60 - z, zz + z, roof, 13)

        mc.setBlocks(xx + 28, yy + 60 - z, zz - z, xx + 28, yy + 60 - z, zz + z, block)
        mc.setBlocks(xx + 111, yy + 60 - z, zz - z, xx + 111, yy + 60 - z, zz + z, block)

    # back wall (with cross)
    mc.setBlocks(xx + 111, yy + 0, zz - 20, xx + 111, yy + 40, zz + 20, block)
    mc.setBlocks(xx + 111, yy + 2, zz - 1, xx + 111, yy + 38, zz + 1, window, 3)
    mc.setBlocks(xx + 111, yy + 28, zz - 12, xx + 111, yy + 30, zz + 12, window, 3)

    # front wall
    mc.setBlocks(xx + 28, yy + 0, zz + -11, xx + 28, yy + 40, zz + 11, block)
    build_x_arch(xx + 28, yy + 0, zz + 0, width=13, height=20, block=AIR)

    # rose window
    for rad in range(8):
        xyz_list = shapes.plan_circle(yy + 30, xx + 28, zz + 0, rad)
        for (x, y, z) in xyz_list:
            mc.setBlock(y, x, z, window, 3)

####################################################################################################
def main():
    AIR = 0
    STONE = 4
    GLOWSTONE = 89

    block = GLOWSTONE
    window = PANE
    roof = WOOL
    if (len(sys.argv) >= 2) and (sys.argv[1] == 'clear'):
        block = AIR
        window = AIR
        roof = AIR

    build_cathedral(0, 0, 0, block, window, roof)

####################################################################################################
if __name__ == '__main__':
    main()

####################################################################################################
