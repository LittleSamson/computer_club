from tkinter import *
tk = Tk()

arena_width  = 500
arena_height = 400
player_size  =  10
step_size    =   3

canvas = Canvas(tk, width=arena_width, height=arena_height)
canvas.pack()

global p1x, p1y, p2x, p2y
p1x = 10 + player_size / 2
p1y = 10 + player_size / 2

p1 = canvas.create_rectangle(10, 10, 10 + player_size, 10 + player_size, fill='blue')

p2x = arena_width - 10 - player_size / 2
p2y = arena_height - 10 - player_size / 2

p2 = canvas.create_rectangle(arena_width - 10 - player_size, arena_height - 10 - player_size, arena_width - 10, arena_height - 10, fill='red')

def move_p1(event):
    global p1x, p1y
    if event.keysym.lower() == 'w':
        canvas.move(p1, 0, -step_size)
        p1y -= step_size
    elif event.keysym.lower() == 'a':
        canvas.move(p1, -step_size, 0)
        p1x -= step_size
    elif event.keysym.lower() == 's':
        canvas.move(p1, 0, step_size)
        p1y += step_size
    elif event.keysym.lower() == 'd':
        canvas.move(p1, step_size, 0)
        p1x += step_size
    canvas.update()

canvas.bind_all('<KeyPress-w>', move_p1)
canvas.bind_all('<KeyPress-a>', move_p1)
canvas.bind_all('<KeyPress-s>', move_p1)
canvas.bind_all('<KeyPress-d>', move_p1)

def move_p2(event):
    global p2x, p2y
    if event.keysym.lower() == 'up':
        canvas.move(p2, 0, -step_size)
        p2y -= step_size
    elif event.keysym.lower() == 'left':
        canvas.move(p2, -step_size, 0)
        p2x -= step_size
    elif event.keysym.lower() == 'down':
        canvas.move(p2, 0, step_size)
        p2y += step_size
    elif event.keysym.lower() == 'right':
        canvas.move(p2, step_size, 0)
        p2x += step_size
    canvas.update()

canvas.bind_all('<KeyPress-Up>', move_p2)
canvas.bind_all('<KeyPress-Left>', move_p2)
canvas.bind_all('<KeyPress-Down>', move_p2)
canvas.bind_all('<KeyPress-Right>', move_p2)

def attack_p1(event):
    global p1x, p1y
    canvas.create_line(p1x, p1y, arena_width, p1y, fill='blue')
    canvas.update()

def attack_p2(event):
    global p2x, p2y
    canvas.create_line(p2x, p2y, 0, p2y, fill='red')
    canvas.update()

canvas.bind_all('<KeyPress-1>', attack_p1)
canvas.bind_all('<KeyPress-0>', attack_p2)

input()
