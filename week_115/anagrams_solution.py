# Steps:
# 1. Make a function that takes a word and returns the sorted version of it
# 2. Read in the word list file
# 3. Construct a dict mapping sorted words to lists of anagrams
# 4. Ask the user for a word, and look up its sorted version in the dict
#    to find any anagrams.

# Ideas to reinforce in advance:
# lists
#   []
#   append
#   del
#   join
#   list
#   sort
#   in
#   referencing
#
# dictionaries
#   {}
#   referencing
#   del
#   putting new stuff in the dictionary

###############
def sort_word(word):
    """Take a string and return a new string with the characters
    presented in alphabetical order"""
    letter_list = list(word)
    letter_list.sort()
    return ''.join(letter_list)

# Read the dictionary file
# And turn it into a list of words
word_text = open('words.txt', 'r').read()
word_list = word_text.splitlines()

# Construct a dictionary mapping each string of sorted letters
# to a list of strings of which it is an anagram
sort_map = {}
for word in word_list:
    sorted_word = sort_word(word)
    if sorted_word not in sort_map:
        sort_map[sorted_word] = []
    sort_map[sorted_word].append(word)

# Keep asking the player to specify a word
while True:
    # Get the next word, and convert it to uppercase
    raw = input("Give me a word, and I'll tell you its anagrams! ")
    raw = raw.upper()
    # Compute the sorted version of the user's word, and look for it in the
    # list of words
    raw_sorted = sort_word(raw)
    if raw_sorted in sort_map:
        print(sort_map[raw_sorted])
    else:
        print("Hmm... I don't know any anagrams of that word...")
    
