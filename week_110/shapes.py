import math
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

#################################################################################
def plan_circle(cenx, ceny, cenz, rad,
                ang_min=0, ang_max=360, ang_step=1):
    """Construct a list of coordinates belonging to a circle in a horizontal
    plane with center (cenx, ceny, cenz) and radius rad. Return them as a list of
    (x, y, z) tuples.

    Required arguments:
        cenx: the x-coordinate of the center of the circle
        ceny: the y-coordinate of the center of the circle
        cenz: the z-coordinate of the center of the circle
        rad : the radius of the circle

    Optional arguments:
        ang_min : the minimum angle of the circle
        ang_max : the maximum angle of the circle
        ang_step: the number of degrees by which the angle steps

    Return value:
        a list of (x, y, z) coordinates belong to the circle
    """

    # The list of coordinates to be returned
    xyz_list = []

    # step around the edge of the circle
    for theta in range(ang_min, ang_max, ang_step):
        # convert the angle theta from degrees to radians
        thetar = math.pi * 2.0 * float(theta) / 360.0

        # construct the coordinates of the circle at this angle
        x = cenx + rad * math.cos(thetar)
        z = cenz + rad * math.sin(thetar)
        y = ceny

        # Round the coordinates to integer values and make a tuple of them
        xyz = (round(x), round(y), round(z))

        # Add these coordinates to the list if we haven't seen them before
        if xyz not in xyz_list:
            xyz_list.append(xyz)

    # Return the list of coordinate tuples to the caller
    return xyz_list

#################################################################################
def build_circle(cenx, ceny, cenz, rad, block,
                 ang_min=0, ang_max=360, ang_step=1):
    """Build a circle in a horizontal plane.

    Required arguments:
        cenx : the x-coordinate of the center of the circle
        ceny : the y-coordinate of the center of the circle
        cenz : the z-coordinate of the center of the circle
        rad  : the radius of the circle
        block: the type of block of which the circle is to be made

    Optional arguments:
        ang_min : the minimum angle of the circle
        ang_max : the maximum angle of the circle
        ang_step: the number of degrees by which the angle steps

    Return value:
        the (x, y, z) coordinates of the center of the circle
    """

    # Build a list of unique coordinates on the circle
    xyz_list = plan_circle(cenx, ceny, cenz, rad, ang_min, ang_max, ang_step)

    # Build each block in the list
    for xyz in xyz_list:
        mc.setBlock(xyz[0], xyz[1], xyz[2], block)

    # Return the center of the circle
    return (cenx, ceny, cenz)

#################################################################################
def build_circle_in_the_sky(cenx, cenz, height, rad, block,
                            ang_min=0, ang_max=360, ang_step=1):
    """Build a circle in a horizontal plane in the sky

    Required arguments:
        cenx  : the x-coordinate of the center of the circle
        cenz  : the z-coordinate of the center of the circle
        height: the distance between the center of the circle and the height of
                the ground at (cenx, cenz)
        rad   : the radius of the circle
        block : the type of block of which the circle is to be made

    Optional arguments:
        ang_min : the minimum angle of the circle
        ang_max : the maximum angle of the circle
        ang_step: the number of degrees by which the angle steps

    Return value:
        the (x, y, z) coordinates of the center of the circle
    """

    # height of the circle in the sky
    ceny = mc.getHeight(cenx, cenz) + rad + height

    return build_circle(cenx, ceny, cenz, rad, height, ang_min, ang_max, ang_step)


#################################################################################
def build_sphere(cenx, ceny, cenz, rad, block,
                 lat_min=0, lat_max=180, lat_step=3,
                 lon_min=0, lon_max=360, lon_step=3):
    """Build a sphere.

    Required arguments:
        cenx : the x-coordinate of the center of the sphere
        ceny : the y-coordinate of the center of the sphere
        cenz : the z-coordinate of the center of the sphere
        rad  : the radius of the sphere
        block: the type of block of which the sphere is to be made

    Optional arguments:
        lat_min : the minimum latitude of the sphere
        lat_max : the maximum latitude of the sphere
        lat_step: the number of degrees by which the latitude steps
        lon_min : the minimum longitude of the sphere
        lon_max : the maximum longitude of the sphere
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the center of the circle
    """

    # teleport to a location above the sphere
    #mc.player.setTilePos(cenx, ceny + 20, cenz)

    #mc.postToChat("Building a sphere!")
    # Go through each latitude...
    for phi in range(lat_min, lat_max, lat_step):
        # Conver the latitude from degrees to radians
        phir = 2.0 * math.pi * float(phi) / 360.0
        # Compute the height of the circle at this latitude
        y = ceny + rad * math.cos(phir)
        # Compute the radius of the circle at this latitude
        radphi = rad * math.sin(phir)
        # Build the circle for this latitude
        build_circle(cenx, y, cenz, radphi, block, lon_min, lon_max, lon_step)
    
    #mc.postToChat("Done!")

    # Return the coordinates of the center of the sphere
    return (cenx, ceny, cenz)

#################################################################################
def build_sphere_in_sky(cenx, cenz, height, rad, block,
                 lat_min=0, lat_max=180, lat_step=3,
                 lon_min=0, lon_max=360, lon_step=3):
    """Build a sphere in the sky

    Required arguments:
        cenx  : the x-coordinate of the center of the sphere
        cenz  : the z-coordinate of the center of the sphere
        height: the distance between the bottom of the sphere and the height of
                the ground at (cenx, cenz)
        rad   : the radius of the sphere
        block : the type of block of which the sphere is to be made

    Optional arguments:
        lat_min : the minimum latitude of the sphere
        lat_max : the maximum latitude of the sphere
        lat_step: the number of degrees by which the latitude steps
        lon_min : the minimum longitude of the sphere
        lon_max : the maximum longitude of the sphere
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the center of the circle
    """

    # calculate the height of the sphere
    ceny = mc.getHeight(cenx, cenz) + rad + height

    return build_sphere(cenx, ceny, cenz, rad, block,
                        lat_min, lat_max, lat_step,
                        lon_min, lon_max, lon_step)

#################################################################################
# build_torus
def build_torus(cenx, ceny, cenz, rad_hor, rad_ver, block,
                 lat_min=0, lat_max=360, lat_step=3,
                 lon_min=0, lon_max=360, lon_step=3):
    """Build a horizontal torus.

    Required arguments:
        cenx   : the x-coordinate of the center of the torus
        ceny   : the y-coordinate of the center of the torus
        cenz   : the z-coordinate of the center of the torus
        rad_hor: the distance from the center of the torus to the center of a
                 cross-section
        rad_ver: the radius of the cross-section of the torus
        block  : the type of block of which the torus is to be made

    Optional arguments:
        lat_min : the minimum latitude of the torus
        lat_max : the maximum latitude of the torus
        lat_step: the number of degrees by which the latitude torus
        lon_min : the minimum longitude of the torus
        lon_max : the maximum longitude of the torus
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the center of the circle
    """

#    # Calculate the height of the torus in the sky
#    ceny = mc.getHeight(cenx, cenz) + rad_ver + height

    # warp to center of the torus
    mc.player.setPos(cenx, ceny + 10, cenz)

    # build the torus
    mc.postToChat('Building doughnut!')
    # Traverse around the doughnut vertically (latitude)
    for phi in range(lat_min, lat_max, lat_step):
        # Convert the latitude angle from degrees to radians
        phir = 2.0 * math.pi * phi / 360.0
        # Compute the height of the circle at this latitude
        y = ceny + rad_ver * math.sin(phir)
        # Compute the radius of the circle at this latitude
        rad = rad_hor + rad_ver * math.cos(phir)
        # Build the circle at this latitude
        build_circle(cenx, y, cenz, rad, block, lon_min, lon_max, lon_step)
        
    mc.postToChat('Done!')

    # Return the coordinates of the center of the torus
    return (cenx, ceny, cenz)

#################################################################################
def build_torus_in_sky(cenx, cenz, height, rad_hor, rad_ver, block,
                       lat_min=0, lat_max=360, lat_step=3,
                       lon_min=0, lon_max=360, lon_step=3):
    """Build a horizontal torus in the sky.

    Required arguments:
        cenx   : the x-coordinate of the center of the torus
        cenz   : the z-coordinate of the center of the torus
        height : the distance between the bottom of the torus and the height of
                 the ground at (cenx, cenz)
        rad_hor: the distance from the center of the torus to the center of a
                 cross-section
        rad_ver: the radius of the cross-section of the torus
        block  : the type of block of which the torus is to be made

    Optional arguments:
        lat_min : the minimum latitude of the torus
        lat_max : the maximum latitude of the torus
        lat_step: the number of degrees by which the latitude torus
        lon_min : the minimum longitude of the torus
        lon_max : the maximum longitude of the torus
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the center of the circle
    """

    # Calculate the height of the torus in the sky
    ceny = mc.getHeight(cenx, cenz) + rad_ver + height

    return build_torus(cenx, ceny, cenz, rad_hor, rad_ver, block,
                       lat_min, lat_max, lat_step,
                       lon_min, lon_max, lon_step)

#################################################################################
def build_cylinder(cenx, ceny, cenz, rad, length, block,
                   lon_min=0, lon_max=360, lon_step=3):
    """Build a vertical cylinder.

    Required arguments:
        cenx  : the x-coordinate of the bottom-center of the cylinder
        ceny  : the y-coordinate of the bottom-center of the cylinder
        cenz  : the z-coordinate of the bottom-center of the cylinder
        rad   : the radius of the circular cross-section of the cylinder
        length: the length of the cylinder from end to end
        block : the type of block of which the cylinder is to be made

    Optional arguments:

        lon_min : the minimum longitude of the cylinder
        lon_max : the maximum longitude of the cylinder
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the bottom-center of the cylinder
    """

    for y in range(ceny, ceny + length):
        build_circle(cenx, y, cenz, rad, block, lon_min, lon_max, lon_step)
        
    return (cenx, ceny, cenz)

#################################################################################
def build_cylinder_in_sky(cenx, cenz, height, rad, length, block,
                          lon_min=0, lon_max=360, lon_step=3):
    """Build a vertical cylinder in the sky.

    Required arguments:
        cenx  : the x-coordinate of the bottom-center of the cylinder
        cenz  : the z-coordinate of the bottom-center of the cylinder
        height: the distance between the bottom of the cylinder and the height of
                the ground at (cenx, cenz)
        rad   : the radius of the circular cross-section of the cylinder
        length: the length of the cylinder from end to end
        block : the type of block of which the cylinder is to be made

    Optional arguments:

        lon_min : the minimum longitude of the cylinder
        lon_max : the maximum longitude of the cylinder
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the bottom-center of the cylinder
    """
    
    # Calculate the height of the bottom of the cylinder
    ceny = mc.getHeight(cenx, cenz) + height

    return build_cylinder(cenx, ceny, cenz, rad, length, block,
                          lon_min, lon_max, lon_step)

#################################################################################
def build_rainbow_cylinder(cenx, ceny, cenz, rad, length,
                           lon_min=0, lon_max=360, lon_step=3):
    """Build a vertical rainbow cylinder.

    Required arguments:
        cenx  : the x-coordinate of the bottom-center of the cylinder
        ceny  : the y-coordinate of the bottom-center of the cylinder
        cenz  : the z-coordinate of the bottom-center of the cylinder
        rad   : the radius of the circular cross-section of the cylinder
        length: the length of the cylinder from end to end

    Optional arguments:

        lon_min : the minimum longitude of the cylinder
        lon_max : the maximum longitude of the cylinder
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the bottom-center of the cylinder
    """
    xyz_list = plan_circle(cenx, ceny, cenz, rad, lon_min, lon_max, lon_step)

    wool = 35
    
    color_map = {
        'white': 0,
        'orange': 1,
        'magenta': 2,
        'light blue': 3,
        'yellow': 4,
        'lime': 5,
        'pink': 6,
        'gray': 7,
        'light gray': 8,
        'cyan': 9,
        'purple': 10,
        'blue': 11,
        'brown': 12,
        'green': 13,
        'red': 14,
        'black': 15,
    }

    color_list = ['red', 'orange', 'yellow', 'green', 'blue', 'cyan', 'purple']
    
    for y in range(length):
        for index, xyz in enumerate(xyz_list):
            color = color_list[(y + index) % len(color_list)]
            color_int = color_map[color]
            mc.setBlock(xyz[0], xyz[1] + y, xyz[2], wool, color_int)
        
    return (cenx, ceny, cenz)

#################################################################################
def build_rainbow_cylinder_in_sky(cenx, cenz, height, rad, length,
                                  lon_min=0, lon_max=360, lon_step=3):
    """Build a vertical cylinder in the sky.

    Required arguments:
        cenx  : the x-coordinate of the bottom-center of the cylinder
        cenz  : the z-coordinate of the bottom-center of the cylinder
        height: the distance between the bottom of the cylinder and the height of
                the ground at (cenx, cenz)
        rad   : the radius of the circular cross-section of the cylinder
        length: the length of the cylinder from end to end

    Optional arguments:

        lon_min : the minimum longitude of the cylinder
        lon_max : the maximum longitude of the cylinder
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the bottom-center of the cylinder
    """

    # Calculate the height of the bottom of the cylinder
    ceny = mc.getHeight(cenx, cenz) + height

    return build_rainbow_cylinder(cenx, ceny, cenz, rad, length,
                                  lon_min, lon_max, lon_step)

#################################################################################
def build_cone(cenx, ceny, cenz, length, slope, block,
               rad_min=0, lon_min=0, lon_max=360, lon_step=3):
    """Build a vertical cone.

    Required arguments:
        cenx  : the x-coordinate of the bottom-center of the cone
        ceny  : the y-coordinate of the bottom-center of the cone
        cenz  : the z-coordinate of the bottom-center of the cone
        length: the length of the cylinder from end to end
        slope : a measure of the steepness of the cone;
                small is steep, and large is broad;
                negative values have the vertex on top, and
                positive values have the vertex on bottom
        block : the type of block of which the cylinder is to be made

    Optional arguments:
        rad_min : the minimum radius of the cone (at the vertex)
        lon_min : the minimum longitude of the cylinder
        lon_max : the maximum longitude of the cylinder
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the bottom-center of the cylinder
    """

    for y in range(length):
        if slope > 0:
            build_circle(cenx, ceny + y, cenz, rad_min + slope * y,
                         block, lon_min, lon_max, lon_step)
        else:
            build_circle(cenx, ceny - y, cenz, rad_min - slope * y,
                         block, lon_min, lon_max, lon_step)
        
    return (cenx, ceny, cenz)

#################################################################################
def build_cone_in_sky(cenx, cenz, height, length, slope, block,
                      rad_min=0, lon_min=0, lon_max=360, lon_step=3):
    """Build a vertical cone.

    Required arguments:
        cenx  : the x-coordinate of the bottom-center of the cone
        cenz  : the z-coordinate of the bottom-center of the cone
        height: the distance between the bottom of the cylinder and the height of
                the ground at (cenx, cenz)
        length: the length of the cylinder from end to end
        slope : a measure of the steepness of the cone;
                small is steep, and large is broad;
                negative values have the vertex on top, and
                positive values have the vertex on bottom
        block : the type of block of which the cylinder is to be made

    Optional arguments:
        rad_min : the minimum radius of the cone (at the vertex)
        lon_min : the minimum longitude of the cylinder
        lon_max : the maximum longitude of the cylinder
        lon_step: the number of degrees by which the longitude steps

    Return value:
        the (x, y, z) coordinates of the bottom-center of the cylinder
    """
    
    # Calculate the height of the bottom of the cone
    ceny = mc.getHeight(cenx, cenz) + height

    return build_cone(cenx, ceny, cenz, length, slope,
                      rad_min, lon_min, lon_max, lon_step)

#################################################################################
def build_prism(xmin, ymin, zmin, xmax, ymax, zmax, block):
    mc.setBlocks(xmin, ymin, zmin,
                 xmax, ymax, zmax, block)
    mc.setBlocks(xmin + 1, ymin + 1, zmin + 1,
                 xmax - 1, ymax    , zmax - 1, 0) # air
#                 xmax - 1, ymax - 1, zmax - 1, 0) # air
    #mc.setBlock(xmin + 1, ymin + 1, zmin, 50) # torch
    #mc.setBlock(xmin + 1, ymin + 1, zmin + 1, 62) # burning furnace
    #mc.setBlock(xmin + 1, ymin + 1, zmin + 1, 51) # fire
