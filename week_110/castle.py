from mcpi.minecraft import Minecraft
mc = Minecraft.create()
import shapes

def build_tower(cenx, ceny, cenz):
    cyl_ht = 20
    radius =  5
    con_ht = 10
    shapes.build_cylinder(cenx, ceny, cenz, radius, cyl_ht, 4)
    shapes.build_cone(cenx, ceny + cyl_ht + con_ht - 1, cenz, con_ht, -0.5, 4)

#for x in range(-40, 45, 20):
#    for z in range(-40, 45, 20):
#        build_tower(x, 0, z)

mc.setBlocks(-60, 0, -60, 60, 60, 60, 0)

for x in range(-20, 20, 10):
    for z in range(-20, 20, 10):
        if x == -20 and z == -20:
            continue
        elif x == -20 and z == 10:
            continue
        elif x == 10 and z == -20:
            continue
        elif x == 10 and z == 10:
            continue
        print(x, z)
        shapes.build_prism(x, 0, z, x + 10, 10, z + 10, 4)
build_tower(-15, 0, -15)
build_tower(-15, 0,  15)
build_tower( 15, 0, -15)
build_tower( 15, 0,  15)
mc.player.setTilePos(0, 40, 0)
