import shapes
import buildings
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

#################################################################################

if 0:
    block = 89

    mc.player.setTilePos(160, 100, 160)

    buildings.build_tower(160, 0, 160, block)
    buildings.build_tower(240, 0, 160, block)
    buildings.build_tower(160, 0, 240, block)
    buildings.build_tower(240, 0, 240, block)

    #buildings.build_tower(200, 0, 200, block)
    shapes.build_pyramid(200, -1, 200, 20, block)

    buildings.build_x_wall(160, 160, 240, 0, 20, block)
    buildings.build_x_wall(240, 160, 240, 0, 20, block)
    buildings.build_z_wall(160, 160, 240, 0, 20, block)
    buildings.build_z_wall(240, 160, 240, 0, 20, block)
            

buildings.build_pillared_tower(120, 0, -120, 10, 89)
mc.player.setPos(120, 150, -120)
