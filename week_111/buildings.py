import shapes
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

#################################################################################
def build_tower(cenx, ceny, cenz, block=4):
    cyl_ht = 50
    radius =  5
    con_ht = 10
    shapes.build_cylinder(cenx, ceny, cenz, radius, cyl_ht, block)
    shapes.build_cone(cenx, ceny + cyl_ht + con_ht - 1, cenz, con_ht, -0.5, block)

    # windows
    for ymin in range(ceny, ceny + cyl_ht, 10):
        mc.setBlocks(cenx - radius, ymin, cenz, cenx - radius, ymin + 3, cenz, 0)
        mc.setBlocks(cenx + radius, ymin, cenz, cenx + radius, ymin + 3, cenz, 0)
        mc.setBlocks(cenx, ymin, cenz - radius, cenx, ymin + 3, cenz - radius, 0)
        mc.setBlocks(cenx, ymin, cenz + radius, cenx, ymin + 3, cenz + radius, 0)

     # interior stairway
    shapes.build_spiral(cenx, ceny, cenz, radius - 2, radius - 1, cyl_ht, block, delta_y=0.05)

     # exterior stairway
    shapes.build_spiral(cenx, ceny, cenz, radius + 1, radius + 2, cyl_ht, block)

    # top
    shapes.build_disk(cenx, ceny + cyl_ht, cenz, radius + 2, block)
    shapes.build_circle(cenx, ceny + cyl_ht + 1, cenz, radius + 2, block, ang_step=30)

#################################################################################
def build_x_wall(x, zmin, zmax, ymin, ymax, block):

    # build the bulk of the wall
    mc.setBlocks(x, ymin, zmin, x, ymax, zmax, block)

    # build the crenelation on top of the wall
    for z in range(zmin, zmax + 1, 2):
        mc.setBlock(x, ymax + 1, z, block)

    # build the indows
    for y in range(ymin, ymax - 5, 10):
        for z in range(zmin, zmax, 10):
            mc.setBlocks(x, y + 2, z, x, y + 5, z, 0)

#################################################################################
def build_z_wall(z, xmin, xmax, ymin, ymax, block):

    # build the bulk of the wall
    mc.setBlocks(xmin, ymin, z, xmax, ymax, z, block)

    # build the crenelation on top of the wall
    for x in range(xmin, xmax + 1, 2):
        mc.setBlock(x, ymax + 1, z, block)

    # build the indows
    for y in range(ymin, ymax - 5, 10):
        for x in range(xmin, xmax, 10):
            mc.setBlocks(x, y + 2, z, x, y + 5, z, 0)

#################################################################################
def build_pillared_tower(cenx, ceny, cenz, rad, block, pillar_height=6, floor_count=8, ang_step=40):

    shapes.build_disk(cenx, ceny, cenz, rad, block)

    for level in range(floor_count):
        for y in range(pillar_height):
            shapes.build_circle(cenx, ceny, cenz, rad, block, ang_step=ang_step)
            ceny += 1
        shapes.build_disk(cenx, ceny, cenz, rad, block)

