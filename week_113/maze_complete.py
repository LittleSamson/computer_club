import os
import time
from mcpi.minecraft import Minecraft
mc = Minecraft.create()


maze0 = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

maze1 = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

maze2 = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

maze = maze2
clear = False

wool = 35
diamond = 57
if clear:
    wool = 0
    diamond = 0

width = len(maze)
length = len(maze[0])
start = (0, 20, 0)

mc.setBlocks(start[0] - 1    , start[1]    , start[2] - 1     ,
             start[0] + width, start[1] + 2, start[2] - 1     , wool)
mc.setBlocks(start[0] - 1    , start[1]    , start[2] + length,
             start[0] + width, start[1] + 2, start[2] + length, wool)
mc.setBlocks(start[0] - 1    , start[1]    , start[2] - 1     ,
             start[0] - 1    , start[1] + 2, start[2] + length, wool)
mc.setBlocks(start[0] + width, start[1]    , start[2] - 1     ,
             start[0] + width, start[1] + 2, start[2] + length, wool)


good_coordinates = []
bad_coordinates = []
win_coordinates = []
for row in range(len(maze)):
    coord_row = []
    for col in range(len(maze)):
        xyz = (start[0] + row, start[1], start[2] + col)
        mc.setBlock(xyz[0], xyz[1], xyz[2], wool, 0)
        if maze[row][col] == 0:
            bad_coordinates.append(xyz)
        elif maze[row][col] == 1:
            good_coordinates.append(xyz)
        elif maze[row][col] == 2:
            win_coordinates.append(xyz)
            mc.setBlock(xyz[0], xyz[1], xyz[2], diamond)
            mc.setBlock(xyz[0], xyz[1] - 1, xyz[2], wool, 0)

if clear:
    os.sys.exit(0)

mc.player.setTilePos(start[0], start[1] + 1, start[2])
while True:
    pos = mc.player.getTilePos()
    xyz = (pos.x, pos.y - 1, pos.z)
    if xyz in good_coordinates:
        mc.setBlock(xyz[0], xyz[1], xyz[2], 35, 13)
    elif xyz in bad_coordinates:
        mc.setBlock(xyz[0], xyz[1], xyz[2], 35, 14)
        mc.player.setTilePos(start[0], start[1] + 1, start[2])
        mc.postToChat("That space is forbidden!  Please try again.")
    elif xyz in win_coordinates:
        mc.setBlock(xyz[0], xyz[1], xyz[2], 0)
        mc.postToChat("You win!  Nice job!")
        break
    time.sleep(0.1)
