import os
import time
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

# Possible mazes
# 0: forbidden space
# 1: acceptable space
# 2: winner space

# NOTE: the upper-left corner is the start of the maze
#       make sure that it is a 1!!!

maze0 = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

maze1 = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

maze2 = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

# Corner of the maze (x, y, z) coordinates
start = (0, 20, 0)

# Select which maze; set clear to True to clear the most recent maze
maze = maze2
clear = False

# set block types
wool = 35
diamond = 57
if clear:
    wool = 0
    diamond = 0

# Build the walls of the maze
width = len(maze)
length = len(maze[0])

mc.setBlocks(start[0] - 1    , start[1]    , start[2] - 1     ,
             start[0] + width, start[1] + 2, start[2] - 1     , wool)
mc.setBlocks(start[0] - 1    , start[1]    , start[2] + length,
             start[0] + width, start[1] + 2, start[2] + length, wool)
mc.setBlocks(start[0] - 1    , start[1]    , start[2] - 1     ,
             start[0] - 1    , start[1] + 2, start[2] + length, wool)
mc.setBlocks(start[0] + width, start[1]    , start[2] - 1     ,
             start[0] + width, start[1] + 2, start[2] + length, wool)

# Set up the coordinates: good, bad, and winner
good_coordinates = []
bad_coordinates = []
win_coordinates = []
for row in range(len(maze)):
    coord_row = []
    for col in range(len(maze)):
        xyz = (start[0] + row, start[1], start[2] + col)
        mc.setBlock(xyz[0], xyz[1], xyz[2], wool, 0)
        if maze[row][col] == 0:
            bad_coordinates.append(xyz)
        elif maze[row][col] == 1:
            good_coordinates.append(xyz)
        elif maze[row][col] == 2:
            win_coordinates.append(xyz)

            # Put a diamond at the winner space
            mc.setBlock(xyz[0], xyz[1], xyz[2], diamond)
            mc.setBlock(xyz[0], xyz[1] - 1, xyz[2], wool, 0)

# Quit early if you're just clearing out the last maze
if clear:
    os.sys.exit(0)

# Teleport to the start of the maze
mc.player.setTilePos(start[0], start[1] + 1, start[2])

#######################################
# TODO: program the logic of the game #
#######################################

