#######################################################################
# Setup: modules, variables, maps
import math, struct

# The number of seconds in a minute!
secs_per_min = 60.0

# A dict mapping note names to numerical frequency values (in Hz)
# e.g., A4 corresponds to 440 Hz
note_map = {
     'C0':   16.35,
    'C#0':   17.32,
    'Db0':   17.32,
     'D0':   18.35,
    'D#0':   19.45,
    'Eb0':   19.45,
     'E0':   20.60,
     'F0':   21.83,
    'F#0':   23.12,
    'Gb0':   23.12,
     'G0':   24.50,
    'G#0':   25.96,
    'Ab0':   25.96,
     'A0':   27.50,
    'A#0':   29.14,
    'Bb0':   29.14,
     'B0':   30.87,
     'C1':   32.70,
    'C#1':   34.65,
    'Db1':   34.65,
     'D1':   36.71,
    'D#1':   38.89,
    'Eb1':   38.89,
     'E1':   41.20,
     'F1':   43.65,
    'F#1':   46.25,
    'Gb1':   46.25,
     'G1':   49.00,
    'G#1':   51.91,
    'Ab1':   51.91,
     'A1':   55.00,
    'A#1':   58.27,
    'Bb1':   58.27,
     'B1':   61.74,
     'C2':   65.41,
    'C#2':   69.30,
    'Db2':   69.30,
     'D2':   73.42,
    'D#2':   77.78,
    'Eb2':   77.78,
     'E2':   82.41,
     'F2':   87.31,
    'F#2':   92.50,
    'Gb2':   92.50,
     'G2':   98.00,
    'G#2':  103.83,
    'Ab2':  103.83,
     'A2':  110.00,
    'A#2':  116.54,
    'Bb2':  116.54,
     'B2':  123.47,
     'C3':  130.81,
    'C#3':  138.59,
    'Db3':  138.59,
     'D3':  146.83,
    'D#3':  155.56,
    'Eb3':  155.56,
     'E3':  164.81,
     'F3':  174.61,
    'F#3':  185.00,
    'Gb3':  185.00,
     'G3':  196.00,
    'G#3':  207.65,
    'Ab3':  207.65,
     'A3':  220.00,
    'A#3':  233.08,
    'Bb3':  233.08,
     'B3':  246.94,
     'C4':  261.63,
    'C#4':  277.18,
    'Db4':  277.18,
     'D4':  293.66,
    'D#4':  311.13,
    'Eb4':  311.13,
     'E4':  329.63,
     'F4':  349.23,
    'F#4':  369.99,
    'Gb4':  369.99,
     'G4':  392.00,
    'G#4':  415.30,
    'Ab4':  415.30,
     'A4':  440.00,
    'A#4':  466.16,
    'Bb4':  466.16,
     'B4':  493.88,
     'C5':  523.25,
    'C#5':  554.37,
    'Db5':  554.37,
     'D5':  587.33,
    'D#5':  622.25,
    'Eb5':  622.25,
     'E5':  659.25,
     'F5':  698.46,
    'F#5':  739.99,
    'Gb5':  739.99,
     'G5':  783.99,
    'G#5':  830.61,
    'Ab5':  830.61,
     'A5':  880.00,
    'A#5':  932.33,
    'Bb5':  932.33,
     'B5':  987.77,
     'C6': 1046.50,
    'C#6': 1108.73,
    'Db6': 1108.73,
     'D6': 1174.66,
    'D#6': 1244.51,
    'Eb6': 1244.51,
     'E6': 1318.51,
     'F6': 1396.91,
    'F#6': 1479.98,
    'Gb6': 1479.98,
     'G6': 1567.98,
    'G#6': 1661.22,
    'Ab6': 1661.22,
     'A6': 1760.00,
    'A#6': 1864.66,
    'Bb6': 1864.66,
     'B6': 1975.53,
     'C7': 2093.00,
    'C#7': 2217.46,
    'Db7': 2217.46,
     'D7': 2349.32,
    'D#7': 2489.02,
    'Eb7': 2489.02,
     'E7': 2637.02,
     'F7': 2793.83,
    'F#7': 2959.96,
    'Gb7': 2959.96,
     'G7': 3135.96,
    'G#7': 3322.44,
    'Ab7': 3322.44,
     'A7': 3520.00,
    'A#7': 3729.31,
    'Bb7': 3729.31,
     'B7': 3951.07,
     'C8': 4186.01,
    'C#8': 4434.92,
    'Db8': 4434.92,
     'D8': 4698.63,
    'D#8': 4978.03,
    'Eb8': 4978.03,
     'E8': 5274.04,
     'F8': 5587.65,
    'F#8': 5919.91,
    'Gb8': 5919.91,
     'G8': 6271.93,
    'G#8': 6644.88,
    'Ab8': 6644.88,
     'A8': 7040.00,
    'A#8': 7458.62,
    'Bb8': 7458.62,
     'B8': 7902.13,
}


# A dict mapping volume names to numerical values
volume_map = {
    'rest':  0.0,
    'pppp':  1.0,
    'ppp' :  2.0,
    'pp'  :  3.0,
    'p'   :  4.0,
    'mp'  :  5.0,
    'mf'  :  6.0,
    'f'   :  7.0,
    'ff'  :  8.0,
    'fff' :  9.0,
    'ffff': 10.0,
}

# A dict mapping names of note lengths to numerical values
length_map = {
    'none'          : 0.0,
    'sixteenth'     : 0.25,
    'eighth'        : 0.5,
    'dotted eighth' : 0.75,
    'quarter'       : 1.0,
    'dotted quarter': 1.5,
    'half'          : 2.0,
    'dotted half'   : 3.0,
    'whole'         : 4.0,
}

#######################################################################
class Note(object):
    """A class for holding the information about a note"""
    ###################################################################
    def __init__(self, start_measure, start_beat, length='quarter', name=None, volume='p', freq=None, channel=1):
        """
        Generates a note based on the input parameters:
        - start_measure - an integer number indicating the measure in which the note starts
        - start_beat    - an integer number indicating the beat in the measure at which the note starts
        - name          - (e.g., 'A4')
        - length        - (e.g., 'quarter', or a number beats)
        - volume        - (e.g., 'mp', or 5.0)
        - freq          - (e.g., 440.0; only considered if 'name' is None)
        - channel       - 1 for left speaker, 2 for right speaker (only if the final audio is to be stereo)
        
        Sets the following attributes of the note:
        - start_measure - the index of the measure to which this note belongs (0-up counting)
        - start_beat    - the beat within the measure at which this note starts (0-up counting)
        - beat_count    - the duration of this note in beats
        - freq          - the frequency of this note in Hz
        - volume        - a numerical value representing the amplitude of this note
        - channel       - the channel to which this note belongs if part of a stereo song (1: left, 2: right)
"""
        # Simply save these three variables in the object
        self.start_measure = start_measure
        self.start_beat    = start_beat
        self.channel       = channel

        # Convert the note name to a floating-point frequency value
        if isinstance(name, str):
#            # convert to uppercase just in case... but breaks for flats (b)
#            name = name.upper()

            if name in note_map:
                self.freq = note_map[name]

#                # shift an octave up...
#                self.freq = note_map[name[:-1] + '0'] * (2.0**(int(name[-1])+1))
            else:
                raise KeyError("Unrecognized note name '%s'" % name)

        # Alternatively, the caller may specify the frequency directly
        elif isinstance(freq, (int, float)):
            self.freq = freq

        else:
            raise Exception("Either the note 'name' or 'freq' is required")
        self.freq = float(self.freq)

        # Convert the note length to a floating point duration (number of beats)
        if isinstance(length, str):
            if length in length_map:
                self.beat_count = length_map[length]
            else:
                raise KeyError("Unrecognized note length '%s'" % length)

        # Alternatively, the caller may have provided a number of beats directly
        elif isinstance(length, (int, float)):
            self.beat_count = length
        else:
            raise Exception("Length input is excepted to be a numerical value (number of beats) or a recognized string but is '%s'" % length)
        self.beat_count = float(self.beat_count)

        # Shorten the note just a little bit to separate adjacent notes
        self.beat_count -= 0.0625

        # Convert the volume to a floating-point value
        if isinstance(volume, str):
            if volume in volume_map:
                self.volume = volume_map[volume]
            else:
                raise KeyError("Unrecognized volume '%s'" % volume)

        # They may have directly provided a numerical volume
        elif isinstance(volume, (int, float)):
            self.volume = volume
        else:
            raise Exception("Volume input is expected to be a numerical value or a recognized string but is '%s'" % volume)
        self.volume = float(self.volume)

    ###################################################################
    def count_samples(self, beats_per_min, samples_per_sec):
        """Count the number of samples occupied by the given note"""
        return int(self.beat_count / beats_per_min * secs_per_min * samples_per_sec)

    ###################################################################
    def generate_samples(self, beats_per_min, samples_per_sec):
        """Generate the samples for the note, and return them"""

        # Count the number of samples occupied by this note,
        # and set up an empty list of samples
        sample_count = self.count_samples(beats_per_min, samples_per_sec)
        samples = [0.0] * sample_count

        # Compute each sample for this note using a sine wave at the appropriate frequency
        for sample_index in range(sample_count):
            samples[sample_index] = self.volume * math.sin(2.0 * math.pi * self.freq * float(sample_index) / float(samples_per_sec))

        # Return the array of samples for this note
        return samples
        

#######################################################################
class Song(object):
    """A class for building a song"""

    ###################################################################
    def __init__(self, beats_per_min=100, beats_per_measure=4):
        """Initialize the song object
        beats_per_min     - defines the tempo of the song
        beats_per_measure - the number of beats in a measure

        Note: the basic beat is assumed to be a quarter note.
        So if beats_per_measure is 3, then the time signature is 3/4"""
        
        # Just save the input parameters as attributes of the object
        self.beats_per_min     = beats_per_min
        self.beats_per_measure = beats_per_measure

        # Initialize an empty array for holding any notes
        self.notes = []

    ###################################################################
    def add_note(self, start_measure, start_beat, length='quarter', name=None, volume='p', channel=1):
        """Construct a note with the given characteristics and add it to the song;
        see the documentation for Note.__init__() for input parameter descriptions"""

        # Make a note object of the appopriate specifications
        note = Note(start_measure, start_beat, length, name, volume, channel=channel)

        # Add it to the list
        self.notes.append(note)

    ###################################################################
    def count_samples(self, samples_per_sec, channel=1):
        """Count the number of samples that are required for the song"""
        # Go through each note and find the one with the latest sample
        sample_count = 0
        for note in self.notes:
            # Find out where this note starts
            start_beat = (self.beats_per_measure * note.start_measure + note.start_beat)
            start_samp = int(start_beat / self.beats_per_min * secs_per_min * samples_per_sec)

            # Find out where this note ends
            end_samp   = start_samp + note.count_samples(self.beats_per_min, samples_per_sec)

            # Update the sample count if this note has the latest ending so far
            sample_count = max(sample_count, end_samp)

        # Return the total number of samples required for this song
        return sample_count

    ###################################################################
    def generate_samples(self, samples_per_sec, channel=1):
        """Generate the array of samples for this song
        samples_per_sec - the number of samples per second in this song
        channel         - only notes with this channel number will be
                          generated."""
        # Count the number of samples required for this song, and
        # Create an empty array of samples
        sample_count = self.count_samples(samples_per_sec, channel)
        samples = [0.0] * sample_count

        # Go through each note in the song, adding it at to the appropriate samples
        for note in self.notes:
            # Only add this note if it's the appropriate channel
            if channel == note.channel:
                # Find out where this note starts
                start_beat = (self.beats_per_measure * note.start_measure + note.start_beat)
                start_samp = int(start_beat / self.beats_per_min * secs_per_min * samples_per_sec)

                # Find out where this note ends
                end_samp   = start_samp + note.count_samples(self.beats_per_min, samples_per_sec)

                # Make the samples for this note
                note_samples = note.generate_samples(self.beats_per_min, samples_per_sec)

                # Add this note's samples to the appropriate point in the song
                for sample_index in range(len(note_samples)):
                    samples[start_samp + sample_index] += note_samples[sample_index]

        # Return the array of constructed samples
        return samples

    ###################################################################
    def write_wav(self, file_name, sample_rate=8000, sample_width=4, n_channels=1):
        """Write a an audio (WAV) file for this song
        file_name    - the name of the file to be written
        sample_rate  - the number of samples per second
        sample_width - the number of bytes to be used per sample
        n_channels   - the number of channels (1 for mono, 2 for stereo)"""
            
        # Generate the samples for the song

        # Mono
        if n_channels == 1:
            song_samples = self.generate_samples(sample_rate)

        # Stereo
        elif n_channels == 2:
            # Generate the samples for each channel
            song_samples_1 = self.generate_samples(sample_rate, channel=1)
            song_samples_2 = self.generate_samples(sample_rate, channel=2)

            # Interleave the samples for the two channels into a single array
            song_samples = [0.0] * (len(song_samples_1) + len(song_samples_2))
            for i in range(len(song_samples_1)):
                song_samples[2 * i    ] = song_samples_1[i]
                song_samples[2 * i + 1] = song_samples_2[i]

        else:
            raise ValueError('n_channels is %d but must be 1 for mono or 2 for streao' % n_channels)

        # Pack the samples into a buffer
        song_buf = struct.pack('%sf' % len(song_samples), *song_samples)

        # Open a wav file and write the sample buffer to it
        import wave
        wav = wave.open(file_name, 'wb')
        wav.setframerate(sample_rate)
        wav.setnchannels(n_channels)
        wav.setsampwidth(sample_width)
        wav.writeframes(song_buf)
        wav.close()

    ###################################################################
    def plot_notes(self,):
        """Use the turtle package to make a plot of the notes
        time is on the horizontal axis, and frequency is on the
        vertical axis"""

        # Set up the turtle object
        import turtle
        t = turtle.Pen()
        t.shape('circle')
        t.speed(0)
        t.turtlesize(0.25, 0.25)
        t.pensize(2)
        t.up()

        x_min = -400
        x_max =  400
        y_min = -200
        y_max =  200

        # Determine the maximum beat number and frequency used by the song
        # so that we can scale the image to fit
        max_beat = 1
        max_freq = 1.0
        for note in self.notes:
            start_beat = (self.beats_per_measure * note.start_measure + note.start_beat)
            max_beat = max(max_beat, start_beat)
            max_freq = max(max_freq, note.freq)

        # Plot each note
        for note in self.notes:
            # Go to the right spot
            start_beat = (self.beats_per_measure * note.start_measure + note.start_beat)
            x = x_min + start_beat / max_beat * (x_max - x_min)
            y = y_min + note.freq  / max_freq * (y_max - y_min)
            t.goto(x, y)

            # Choose a different color for each channel (blue for left, red for right)
            if 1 == note.channel:
                t.color('blue')
            else:
                t.color('red')

            # Put a circle at the note start
            t.stamp()

            # Draw a line until the note end
            t.down()
            x_stop = x + note.beat_count / max_beat * (x_max - x_min)
            t.forward(x_stop - x)
            t.up()
#            t.stamp()

        t.hideturtle()
#        input()

#######################################################################
