###################################
# Setup
from music import Song

wav_name          = "twinkle.wav"
beats_per_minute  =   100.0
beats_per_measure =     4
sample_rate       = 80000.0 # samples per second

# Start the song object
song = Song(beats_per_minute, beats_per_measure)

###################################
# Add the notes
song.add_note(0, 0, 1, 'C4') # Twin-
song.add_note(0, 1, 1, 'C4') # kle,
song.add_note(0, 2, 1, 'G4') # twin-
song.add_note(0, 3, 1, 'G4') # kle,
song.add_note(1, 0, 1, 'A4') # lit-
song.add_note(1, 1, 1, 'A4') # tle
song.add_note(1, 2, 2, 'G4') # star;
song.add_note(2, 0, 1, 'F4') # how
song.add_note(2, 1, 1, 'F4') # I
song.add_note(2, 2, 1, 'E4') # won-
song.add_note(2, 3, 1, 'E4') # der
song.add_note(3, 0, 1, 'D4') # what
song.add_note(3, 1, 1, 'D4') # you
song.add_note(3, 2, 2, 'C4') # are.

song.add_note(4, 0, 1, 'G4') # Up
song.add_note(4, 1, 1, 'G4') # a-
song.add_note(4, 2, 1, 'F4') # bove
song.add_note(4, 3, 1, 'F4') # the
song.add_note(5, 0, 1, 'E4') # world
song.add_note(5, 1, 1, 'E4') # so
song.add_note(5, 2, 2, 'D4') # high,
song.add_note(6, 0, 1, 'G4') # like
song.add_note(6, 1, 1, 'G4') # a
song.add_note(6, 2, 1, 'F4') # dia-
song.add_note(6, 3, 1, 'F4') # mond
song.add_note(7, 0, 1, 'E4') # in
song.add_note(7, 1, 1, 'E4') # the
song.add_note(7, 2, 2, 'D4') # sky.

song.add_note(8, 0, 1, 'C4') # Twin-
song.add_note(8, 1, 1, 'C4') # kle
song.add_note(8, 2, 1, 'G4') # Twin-
song.add_note(8, 3, 1, 'G4') # kle
song.add_note(9, 0, 1, 'A4') # lit-
song.add_note(9, 1, 1, 'A4') # tle
song.add_note(9, 2, 2, 'G4') # star;
song.add_note(10, 0, 1, 'F4') # how
song.add_note(10, 1, 1, 'F4') # I 
song.add_note(10, 2, 1, 'E4') # won-
song.add_note(10, 3, 1, 'E4') # der
song.add_note(11, 0, 1, 'D4') # what
song.add_note(11, 1, 1, 'D4') # you
song.add_note(11, 2, 2, 'C4') # are.

###################################
# Write the song to a WAV file
print("Writing the song to '%s'" % wav_name)
song.write_wav(wav_name, sample_rate=sample_rate)

# Plot the notes?
#song.plot_notes()
