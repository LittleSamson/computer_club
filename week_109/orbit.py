import shapes
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

# Make a sphere at (x, z) = (5, 10) hovering 20 blocks above the ground
# its radius is 6, and it's made of stone (4)
# This function returns an (x, y, z) tuple with the center of the sphere
cen = shapes.build_sphere_in_sky(-5, -10, 20, 6, 4)

# Plan a circle of radius 10 that goes around the sphere
xyz_list = shapes.plan_circle(cen[0], cen[1], cen[2], 10)

# Build the orbit
# Task #1: change this loop so that instead of making a solid circle it makes
#          a planet orbiting the sphere
# Task #2: make the planet orbit forever (until the program is ended)
for xyz in xyz_list:
    mc.setBlock(xyz[0], xyz[1], xyz[2], 4)
