def get_card_name(number):
    if number == 1:
        return 'ace'
    elif number == 11:
        return 'jack'
    elif number == 12:
        return 'queen'
    elif number == 13:
        return 'king'
    else:
        return str(number)

def get_card_value(number):
    if number == 1:
        return 1
    elif number == 11:
        return 10
    elif number == 12:
        return 10
    elif number == 13:
        return 10
    else:
        return number

cards = []
for suit in ['spades', 'hearts', 'clubs', 'diamonds']:
    for number in range(1, 14):
        card = {
            'number': number
            'suit'  : suit,
            'name'  : get_card_name(number)
            'value' : get_card_value(number),
        }
        cards.append(card)

for card in cards:
    print(card)
