import random


class Card(object):
    def __init__(self, number, suit):

        # for most cards, the value is the number
        # and the name is the string representation of the number
        self.number = number
        self.value  = number
        self.name   = str(number)
        self.suit   = suit

        # cards 1, 11, 12, and 13 have special names and values
        if number == 1:
            self.name  = 'ace'
        elif number == 11:
            self.name  = 'jack'
            self.value = 10
        elif number == 12:
            self.name  = 'queen'
            self.value = 10
        elif number == 13:
            self.name  = 'king'
            self.value = 10

    def __str__(self,):
        return '%s of %s' % (self.name, self.suit)

class Deck(object):
    def __init__(self,):
        self.cards = []
        for suit in ['spades', 'hearts', 'clubs', 'diamonds']:
            for number in range(1, 14):
                card = Card(number, suit)
                self.cards.append(card)

    def deal_one_card(self, index=0):
        card = self.cards[index]
        del self.cards[index]
        return card

    def shuffle(self,):
        new_cards = []

        while len(self.cards) > 0:
            card_index = random.randint(0, len(self.cards) - 1)
            card = self.deal_one_card(card_index)
            new_cards.append(card)

        self.cards = new_cards

class Hand(object):
    def __init__(self,):
        self.cards = []

    def add_card(self, card):
        self.cards.append(card)
        print("You drew the %s" % str(card))

    def count_points(self,):
        if len(self.cards) == 2:
            if self.cards[0].name == 'ace' and self.cards[1].name in ('jack', 'queen', 'king'):
                return 21
            if self.cards[1].name == 'ace' and self.cards[0].name in ('jack', 'queen', 'king'):
                return 21
        total = 0
        for card in self.cards:
            total += card.value
        return total

    def print_status(self,):
        print('Right now you have these cards:')
        for card in self.cards:
            print(card)
        print('You have %d points.' % self.count_points())
        
deck = Deck()
deck.shuffle()

#for card in d.cards:
#    print(card)

hand = Hand()

first_card = deck.deal_one_card()
second_card = deck.deal_one_card()
hand.add_card(first_card)
hand.add_card(second_card)

print(hand.count_points())
while hand.count_points() < 21:
    hand.print_status()
    action = input("Would you like to hit ('h') or stay (anything else)?")
    if action == 'h':
        card = deck.deal_one_card()
        hand.add_card(card)
    else:
        break

hand.print_status()
points = hand.count_points()
if points > 21:
    print("You bust!")
elif points == 21:
    print("You got 21!  You win!")
else:
    print("You got %d.  Not bad!" % points)
