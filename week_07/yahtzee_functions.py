import random

def roll_a_die():
    number = random.randint(1, 6)
    print ("You rolled a %d!" % number)
    return number


def check_dice(dice):
    
    dice.sort()

    if dice[0] == dice[1] and dice[0] == dice[2] and dice[0] == dice[3] and dice[0] == dice[4]:
        print("Yahtzee")
        return 50

    if (dice[0] + 1) == dice[1] and (dice[1] + 1) == dice[2] and (dice[2] + 1 == dice[3]) and (dice[3] + 1) == dice[4]:
        print("Large straight!")
        return 40

    if (dice[0] + 1) == dice[1] and (dice[1] + 1) == dice[2] and (dice[2] + 1) == dice[3]:
        print("Small straight!")
        return 30

    if (dice[1] + 1) == dice[2] and (dice[2] + 1 == dice[3]) and (dice[3] + 1) == dice[4]:
        print("Small straight!")
        return 30

    if dice[0] == dice[1] and dice[1] == dice[2] and dice[3] == dice[4]:
        print("Full house!")
        return 25

    if dice[0] == dice[1] and dice[2] == dice[3] and dice[3] == dice[3]:
        print("Full house!")
        return 25

    if dice[0] == dice[1] and dice[1] == dice[2] and dice[2] == dice[3]:
        print("Four of a kind!")
        return sum(dice)

    if dice[1] == dice[2] and dice[2] == dice[3] and dice[3] == dice[4]:
        print("Four of a kind!")
        return sum(dice)

    if dice[0] == dice[1] and dice[1] == dice[2]:
        print("Three of a kind!")
        return sum(dice)

    if dice[1] == dice[2] and dice[2] == dice[3]:
        print("Three of a kind!")
        return sum(dice)

    if dice[2] == dice[3] and dice[3] == dice[4]:
        print("Three of a kind!")
        return sum(dice)

    return sum(dice)


dice = []
for i in range(5):
    dice.append(roll_a_die())

dice.sort()
print(dice)
score = check_dice(dice)
print("You have %d points!" % score)
