# Import the turtle package and initialize a Pen object
import turtle
t = turtle.Pen()

####################
# D
# Navigate to the lower-left corner of the outer edge

t.up()
t.back(220)

# Outer edge
t.fillcolor("green")
t.begin_fill()
t.down()
t.forward(10)
t.circle(50, 180)
t.forward(10)
t.left(90)
t.forward(100)
t.up()
t.end_fill()

# Navigate to the lower-left corner of the inner edge
t.left(90)
t.forward(15)
t.left(90)
t.forward(20)
t.right(90)

# Inner edge
t.fillcolor("white")
t.begin_fill()
t.down()
t.circle(30, 180)
t.left(90)
t.forward(60)
t.up()
t.end_fill()

####################
# A
# Navigate to the lower-left corner of the outer edge
t.forward(20)
t.left(90)
t.forward(50)

# Outer edge
t.fillcolor("blue")
t.begin_fill()
t.down()
t.forward(20)
t.left(60)
t.forward(50)
t.right(60)
t.forward(23)
t.right(60)
t.forward(50)
t.left(60)
t.forward(20)
t.left(120)
t.forward(115.47)
t.left(60)
t.forward(0)
t.left(60)
t.forward(115.47)
t.up()
t.end_fill()

# Navigate to lower-left corner of inner edge
t.left(120)
t.forward(52)
t.left(90)
t.forward(55)

# Inner edge
t.fillcolor("white")
t.begin_fill()
t.down()
t.right(90)
t.forward(15)
t.left(120)
t.forward(17)
t.left(120)
t.forward(17)
t.up()
t.end_fill()

####################
# V
# Navigate to the lower-left corner
t.left(30)
t.forward(52)
t.left(90)
t.forward(90)

# (Only) edge
t.fillcolor("green")
t.begin_fill()
t.down()
t.forward(20)
t.left(60)
t.forward(115.47)
t.left(120)
t.forward(20)
t.left(60)
t.forward(95)
t.right(120)
t.forward(95)
t.left(60)
t.forward(20)
t.left(120)
t.forward(115.47)
t.up()
t.end_fill()

####################
# I
# Navigate to the lower-left corner
t.left(60)
t.forward(90)

# (Only) edge
t.fillcolor("blue")
t.begin_fill()
t.down()
t.forward(60)
t.left(90)
t.forward(20)
t.left(90)
t.forward(20)
t.right(90)
t.forward(60)
t.right(90)
t.forward(20)
t.left(90)
t.forward(20)
t.left(90)
t.forward(60)
t.left(90)
t.forward(20)
t.left(90)
t.forward(20)
t.right(90)
t.forward(60)
t.right(90)
t.forward(20)
t.left(90)
t.forward(20)
t.left(90)
t.up()
t.end_fill()

####################
# D
# Navigate to lower-left corner of the outer edge
t.forward(80)

# Outer edge
t.fillcolor("green")
t.begin_fill()
t.down()
t.forward(10)
t.circle(50, 180)
t.forward(10)
t.left(90)
t.forward(100)
t.up()
t.end_fill()

# Navigate to the lower-left corner of the inner edge
t.left(90)
t.forward(15)
t.left(90)
t.forward(20)
t.right(90)

# Inner edge
t.fillcolor("white")
t.begin_fill()
t.down()
t.circle(30, 180)
t.left(90)
t.forward(60)
t.up()
t.end_fill()

# Shoot the turtle off into space!
t.forward(1000)

# Prompt user for input if turtle window disappears when program ends!
input('Press Enter to exit.')

