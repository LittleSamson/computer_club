#########################################################################
# Setup 

# packages to import:
# turtle is for drawing stuff
# sys has a function for exiting the program
import turtle, sys

# Board parameters
bgcolor        = 'black'
color          = 'yellow'
box_width      = 100
line_thickness =   8
font           = ("Arial", 16, "bold")

#########################################################################
# Box class (a Board is made up of 9 of these)
class Box(turtle.Pen):
    
    def __init__(self, center_x, center_y, box_width):
        """Set up a new Box object with the given center and width"""

        # Do all of the usual initializiation for a turtle.Pen object
        super(Box, self).__init__()

        # Save input parameters as attributes of the object
        self.center_x  = center_x
        self.center_y  = center_y
        self.box_width = box_width

        # define the four boundaries of this box
        self.l_edge = center_x - box_width / 2 # left
        self.r_edge = center_x + box_width / 2 # right
        self.b_edge = center_y - box_width / 2 # bottom
        self.t_edge = center_y + box_width / 2 # top

        # Hide the cursor till it's needed!
        self.hideturtle()
  
        # Go to the center of the square!
        self.speed(0)
        self.up()
        self.goto(center_x, center_y)

        # The box starts off empty
        # When it's clicked later this will be 'X' or 'O'
        self.letter = None


    def select(self, letter):
        """ select this box with the given letter if it hasn't been selected yet
        if the selection worked, return True
        if the selection didn't work, return False"""

        # if this box hasn't been selected yet...
        if self.letter is None:
            # update the attribute
            self.letter = letter

            # print the letter
            self.down()
            self.color(color)
            self.write(letter, align='center', font=font)

            # return True to indicate that the selection as successful
            return True

        else:
            # Return False if this box has already been selected
            return False

    def was_clicked(self, x, y):
        """ Determine whether or not the indicated (x, y) coordinates are inside this box"""

        # TODO: return True when (x, y) is inside the box
        return False
        

#########################################################################
# Board class (contains 9 Boxes)
class Board(turtle.Pen):

    def __init__(self, box_width, line_thickness):
        """Set up a 3x3 board with boxes of the indicate width, and
        lines of the indicated thickness"""

        # Do all of the usual initialization for a turtle.Pen object
        super(Board, self).__init__()

        # Get the screen on which the turtle is drawing
        self.screen = self.getscreen()
        
        # Save input parameters as attributes of the object
        self.box_width = box_width
        self.line_thickness = line_thickness

        # Draw the grid
        self.screen.bgcolor(bgcolor)
        self.hideturtle()
        self.shape('circle')
        self.color(color)
        self.speed(0)
        self.pensize(line_thickness)

        w = box_width # short variable name for convenience!
        self.draw_line(    w,     0,     w, 3 * w) # left vertical line
        self.draw_line(2 * w,     0, 2 * w, 3 * w) # right vertical line
        self.draw_line(    0,     w, 3 * w,     w) # bottom horizontal line
        self.draw_line(    0, 2 * w, 3 * w, 2 * w) # top horizontal line

        self.goto(0, -200)
        self.write("Welcome to Tic-Tac-Toe!  Press Escape to exit at any time.", align='center', font=font)
        self.goto(0, -100)

        # Set up the list of boxes that make up the board
        # This is an list of lists: each list in self.boxes represents a row of the grid
        # self.boxes[0][2] is the top-right corner box
        self.boxes = [
            [None, None, None],
            [None, None, None],
            [None, None, None],
        ]
        for row in range(3):
            for col in range(3):
                x =      row  * box_width + box_width / 2 # start at left and work rightward
                y = (3 - col) * box_width - box_width / 2 # start at TOP and work downward
                self.boxes[row][col] = Box(x, y, box_width)

        # This is the next letter to be chosen... X always goes first
        self.next_letter = 'X'
        self.screen.title("Tic Tac Toe ... %s's turn" % self.next_letter)

    def draw_line(self, x_start, y_start, x_stop, y_stop):
        """a helper function!  draw a line from start (x, y) to stop (x, y)"""
        self.up()
        self.goto(x_start, y_start)
        self.down()
        self.goto(x_stop, y_stop)
        self.up()

    def click(self, x, y):
        """go through boxes to see if any of them was clicked and act accrdingly"""

        # loop through rows
        for row in range(3):
            # loop through columns
            for col in range(3):
                # set this box to a local variable for convenience
                box = self.boxes[row][col]

                # Check whether or not this box was clicked...
                if box.was_clicked(x, y):

                    # The user clicked this box; select it if it hasn't be clicked before.
                    is_new_selection = box.select(self.next_letter)
                    if is_new_selection:
                        
                        # See if the latest click causesd somebody to win!
                        # ... or if all of the boxes have now been checked
                        winner            = self.check_winner()
                        num_checked_boxes = self.count_checked_boxes()

                        # if there's a winner...
                        if winner is not None:
                            # report the winner and disable further mouse clicks
                            self.screen.onclick(None)
                            self.write('%s is the winner!' % winner, align='center', font=font)
                            self.screen.title("Tic Tac Toe ... %s is the winner!" % winner)

                        # if all 9 boxes have been checked...
                        elif self.count_checked_boxes() == 9:
                            # report the winner and disable further mouse clicks
                            self.screen.onclick(None)
                            self.write("It's a draw!", align = 'center', font=font)
                            self.screen.title("Tic Tac Toe ... It's a draw!")

                        # Otherwise...
                        else:
                            # no winner yet, and there are more boxes to select, so switch to the next letter
                            if self.next_letter == 'X':
                                self.next_letter = 'O'
                            else:
                                self.next_letter = 'X'
                            self.screen.title("Tic Tac Toe ... %s's turn" % self.next_letter)

    def count_checked_boxes(self,):
        """Go through the boxes and count the ones that are checked (i.e., have an assigned letter)"""

        # This variable keeps track of how many boxes have been checke as you go along
        num_checked = 0

        # TODO: loop through rows and columns and add up any boxes whose letter is not None

        # Return the total number of checked boxes that you counted
        return num_checked

    def check_winner(self,):
        """Determine whether there has been a winner and, if so, who it is.
        Return 'X' or 'O' if one of them one, and return None otherwise."""
        
        # Check the top-left to bottom-right diagonal
        box_a = self.boxes[0][0].letter
        box_b = self.boxes[1][1].letter
        box_c = self.boxes[2][2].letter
        if box_a is not None and box_a == box_b and box_a == box_c:
            return box_a

        # TODO: Check the top-left to bottom-right diagonal, and return the winner if found

        # TODO: Check each row, and return the winner if found
                
        # TODO: Check each column, and return the winner if found

        # No rows, columns, or diagonals had the same value, so no winner (yet)
        return None

#########################################################################
# Set up the board
board = Board(box_width, line_thickness)

# listen for key presses and mouses clicks!
board.screen.listen()

# Bind the board.click() function to mouse clicks
board.screen.onclick(board.click)

# Bind a built-in function for exiting the program to the Escape key
board.screen.onkey(sys.exit, 'Escape')

#########################################################################
# This starts the program main loop
board.screen.mainloop()
