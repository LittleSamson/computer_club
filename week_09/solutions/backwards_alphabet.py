# Get input message from the user
message_in = input('What is your message?\n')

# Convert the message to upper case so that there are fewer things to check
message_in = message_in.upper()

# build a dict mapping input to output letters
# (either direction)
letter_map = {
	'A': 'Z',
	'B': 'Y',
	'C': 'X',
	'D': 'W',
	'E': 'V',
	'F': 'U',
	'G': 'T',
	'H': 'S',
	'I': 'R',
	'J': 'Q',
	'K': 'P',
	'L': 'O',
	'M': 'N',
	'N': 'M',
	'O': 'L',
	'P': 'K',
	'Q': 'J',
	'R': 'I',
	'S': 'H',
	'T': 'G',
	'U': 'F',
	'V': 'E',
	'W': 'D',
	'X': 'C',
	'Y': 'B',
	'Z': 'A',
}	
	
	
# Put together a string that translates each letter from response
message_out = ''
for y in message_in:
	if y in letter_map:
		message_out += letter_map[y]
	else:
		message_out += y

# Print the output message
print(message_out)
