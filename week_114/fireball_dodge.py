"""A fireball game!"""
####################################################################################################
import datetime
import random
import sys
import time
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

####################################################################################################
BOARD_CORNER = (0, 20, 0)
BOARD_WIDTH = 10
BOARD_DEPTH = 20
BOARD_HEIGHT = 5
BOARD_BLOCK = 1 # stone

AIR = 0
LAVA = 11

FIREBALL_COUNT = 10
FIREBALL_MIN_DELAY = 0.02 # seconds
FIREBALL_MAX_DELAY = 0.20 # seconds

####################################################################################################
def flip_coin(heads_probability=0.01):
    """Flip a biased coin with a given probability of "heads"; return True if heads is chosen, and
    False otherwise"""
    random_number = random.uniform(0.0, 1.0)
    return bool(random_number < heads_probability)

####################################################################################################
def build_board(block=BOARD_BLOCK):
    """Build the game board (or delete it by setting block = AIR"""

    # Clear out the game board
    mc.setBlocks(BOARD_CORNER[0]              , BOARD_CORNER[1]               , BOARD_CORNER[2]              ,
                 BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1] + BOARD_HEIGHT, BOARD_CORNER[2] + BOARD_WIDTH, 
                 AIR)

    # make the floor of the board
    # TODO: Make the board


####################################################################################################
## TODO: Make a fireball class

####################################################################################################
def main():
    """A fireball game"""

    #build_board(AIR)
    #sys.exit(0)
    build_board()

    fireball_list = []
    try:
        while True:
            # Add another fireball with some probability (up to the maximum number of fireballs)
            more_fireballs_allowed = bool(len(fireball_list) < FIREBALL_COUNT)
            if more_fireballs_allowed and flip_coin():
                ## TODO: choose a random z-position (larger than 0, smaller than BOARD_WIDTH)
                ## TODO: choose a random delay (from FIREBALL_MIN_DELAY to FIREBALL_MAX_DELAY)
                ## TODO: create one fireball and add it to the list
                pass # DELETEME

            # animate the fireballs, and destroy them if they get to the end
            ## TODO: loop through the fireballs
            ## TODO: Step each one, but it gets to the back wall, delete it

            # Sleep for a moment to give your computer a chance to rest
            time.sleep(0.001)

    except KeyboardInterrupt:
        mc.postToChat("Thanks for playing!")

    # Delete the board.  It's good to clean up after yourself!
    build_board(AIR)

####################################################################################################
if __name__ == '__main__':
    main()
####################################################################################################
