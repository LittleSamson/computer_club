"""A fireball game!"""
####################################################################################################
import datetime
import random
import sys
import time
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

####################################################################################################
BOARD_CORNER = (0, 20, 0)
BOARD_WIDTH = 10
BOARD_DEPTH = 20
BOARD_HEIGHT = 5
BOARD_BLOCK = 1 # stone

AIR = 0
LAVA = 11

FIREBALL_COUNT = 10
FIREBALL_MIN_DELAY = 0.02 # seconds
FIREBALL_MAX_DELAY = 0.20 # seconds

####################################################################################################
def flip_coin(heads_probability=0.01):
    """Flip a biased coin with a given probability of "heads"; return True if heads is chosen, and
    False otherwise"""
    random_number = random.uniform(0.0, 1.0)
    return bool(random_number < heads_probability)

####################################################################################################
def build_board(block=BOARD_BLOCK):
    """Build the game board (or delete it by setting block = AIR"""

    # Clear out the game board
    mc.setBlocks(BOARD_CORNER[0]              , BOARD_CORNER[1]               , BOARD_CORNER[2]              ,
                 BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1] + BOARD_HEIGHT, BOARD_CORNER[2] + BOARD_WIDTH, 
                 AIR)

    # make the floor of the board
    mc.setBlocks(BOARD_CORNER[0]              , BOARD_CORNER[1], BOARD_CORNER[2]              ,
                 BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1], BOARD_CORNER[2] + BOARD_WIDTH, 
                 block)

    return # for now, just make the base.

    # make the walls
      # back wall (behind player)
    mc.setBlocks(BOARD_CORNER[0] - 1, BOARD_CORNER[1]               , BOARD_CORNER[2]              ,
                 BOARD_CORNER[0] - 1, BOARD_CORNER[1] + BOARD_HEIGHT, BOARD_CORNER[2] + BOARD_WIDTH,
                 block)
    
      # back wall (facing player)
    mc.setBlocks(BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1]               , BOARD_CORNER[2]              ,
                 BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1] + BOARD_HEIGHT, BOARD_CORNER[2] + BOARD_WIDTH,
                 block)
    
      # left wall
    mc.setBlocks(BOARD_CORNER[0] - 1          , BOARD_CORNER[1]               , BOARD_CORNER[2],
                 BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1] + BOARD_HEIGHT, BOARD_CORNER[2],
                 block)
    
      # right wall
    mc.setBlocks(BOARD_CORNER[0] - 1          , BOARD_CORNER[1]               , BOARD_CORNER[2] + BOARD_WIDTH,
                 BOARD_CORNER[0] + BOARD_DEPTH, BOARD_CORNER[1] + BOARD_HEIGHT, BOARD_CORNER[2] + BOARD_WIDTH,
                 block)

####################################################################################################
class Fireball(object):
    """A class for describing a fireball"""

    def __init__(self, zpos, wait_seconds, block=LAVA):
        """Initialize the position of the fireball to the back of the board at some z-position with
        some delay. zpos is relative to board, so it should be in the range [1, BOARD_DEPTH - 1]"""
        self.xpos = BOARD_CORNER[0] + BOARD_DEPTH - 1
        self.ypos = BOARD_CORNER[1] + 2
        self.zpos = BOARD_CORNER[2] + zpos
        self.wait = datetime.timedelta(seconds=wait_seconds)
        self.block = block
        mc.setBlock(self.xpos, self.ypos, self.zpos, self.block)
        self.next_move = datetime.datetime.utcnow() + self.wait

    def step(self,):
        """If enough time has elapsed, take one step forward;
        Return False if the fireball hits the wall, and True otherwise"""

        # Check to see whether it's time for this fireball to move!
        now = datetime.datetime.utcnow()
        if now >= self.next_move:

            # Remove the fireball from current position
            mc.setBlock(self.xpos, self.ypos, self.zpos, AIR)

            # If you've run into the wall, don't take any more steps forward
            if self.xpos <= BOARD_CORNER[0]:
                return False

            # Take a step forward, draw the fireball at the new position
            self.xpos -= 1
            mc.setBlock(self.xpos, self.ypos, self.zpos, self.block)

            # Update the time for the next move of the fireball
            self.next_move = datetime.datetime.utcnow() + self.wait

        # Indicate that the function completed successfully
        return True

    def __del__(self,):
        """Make sure that the fireball is deleted from the current position"""
        mc.setBlock(self.xpos, self.ypos, self.zpos, AIR)

####################################################################################################
def main():
    """A fireball game"""

    #build_board(AIR)
    #sys.exit(0)
    build_board()

    fireball_list = []
    try:
        while True:
            # Add another fireball with some probability (up to the maximum number of fireballs)
            more_fireballs_allowed = bool(len(fireball_list) < FIREBALL_COUNT)
            if more_fireballs_allowed and flip_coin():
                zpos = random.randint(1, BOARD_WIDTH - 1)
                wait_seconds = random.uniform(FIREBALL_MIN_DELAY, FIREBALL_MAX_DELAY) # random delay
                fireball = Fireball(zpos, wait_seconds)
                fireball_list.append(fireball)

            # animate the fireballs, and destroy them if they get to the end
            fireball_index = 0
            while fireball_index < len(fireball_list):
                fireball = fireball_list[fireball_index]
                result = fireball.step()
                if not result:
                    del fireball_list[fireball_index]
                else:
                    fireball_index += 1
            time.sleep(0.001)

    except KeyboardInterrupt:
        mc.postToChat("Thanks for playing!")

    # Delete the board.  It's good to clean up after yourself!
    build_board(AIR)

####################################################################################################
if __name__ == '__main__':
    main()
####################################################################################################
