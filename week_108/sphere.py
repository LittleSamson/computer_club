import math
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

# block type
air = 0
stone = 1
block = stone

# radius of the sphere
rad = 15

# center of the sphere
cenx = 40
cenz = 40
ceny = mc.getHeight(cenx, cenz) + rad + 10

# teleport to a location above the sphere
mc.player.setTilePos(cenx, ceny + 20, cenz)

mc.postToChat("Building a sphere!")
#for phi in range(90): # hat
#for phi in range(90, 180): # bowl
#for phi in range(180): # sphere
for phi in range(0, 180, 3):
    phir = 2.0 * math.pi * float(phi) / 360.0
    radphi = rad * math.sin(phir)
    y = ceny + rad * math.cos(phir)
    for theta in range(0, 360, 3):
        thetar = 2.0 * math.pi * float(theta) / 360.0

        x = cenx + radphi * math.cos(thetar)
        z = cenz + radphi * math.sin(thetar)

        mc.setBlock(x, y, z, block)

mc.postToChat("Done!")
