import math
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

# build doughnut out of stone
air = 0
stone = 4
block = stone

rad_hor = 10 # radius of ring
rad_ver = 5 # radius of cross-section

# center of torus
cenx = 0
cenz = 0
ceny = mc.getHeight(cenx, cenz) + 20

# warp to center of doughnut
mc.player.setPos(cenx, ceny + 10, cenz)

# build the doughnut
mc.postToChat('Building doughnut!')
for theta in range(0, 360, 4):
    for phi in range(0, 360, 4):
        theta_rad = 2.0 * math.pi * theta / 360.0
        phi_rad = 2.0 * math.pi * phi / 360.0
        x = cenx + math.cos(theta_rad) * (rad_hor + rad_ver * math.cos(phi_rad))
        z = cenz + math.sin(theta_rad) * (rad_hor + rad_ver * math.cos(phi_rad))
        y = ceny + rad_ver * math.sin(phi_rad)
        mc.setBlock(x, y, z, block)
        
mc.postToChat('Done!')
