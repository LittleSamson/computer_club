def factorial(n):
    if n <= 1:
        return n
    return (n * factorial(n - 1))

while True:
    try:
        x = input("Give me a number, and I will compute its factorial! ")
        x = int(x)
        x_factorial = factorial(x)
        print("%d! = %d" % (x, x_factorial))
    except ValueError:
        print("Problem converting input to an integer!")

    
