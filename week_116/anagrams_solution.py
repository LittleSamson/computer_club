import datetime


letter_points = {
    'A': 1,
    'B': 3,
    'C': 3,
    'D': 2,
    'E': 1,
    'F': 4,
    'G': 2,
    'H': 4,
    'I': 1,
    'J': 8,
    'K': 5,
    'L': 1,
    'M': 3,
    'N': 1,
    'O': 1,
    'P': 3,
    'Q': 10,
    'R': 1,
    'S': 1,
    'T': 1,
    'U': 1,
    'V': 4,
    'W': 4,
    'X': 8,
    'Y': 4,
    'Z': 10,
}


def sort_word(word):
    # PURPLE -> ELPPRU
    word = word.upper()
    word_list = list(word)
    word_list.sort()
    return ''.join(word_list)

print(sort_word('purple'))

words_text = open('words.txt', 'r').read()
word_list = words_text.split()

a_to_z = {}
for word in word_list:
    alph = sort_word(word)
    if alph not in a_to_z:
        a_to_z[alph] = []
    a_to_z[alph].append(word)

def score_word(word):
    """"""
    score = 0
    for letter in word.upper():
        score += letter_points[letter]
    return score
    

def get_sorted_substrings(word):
    """"""
    substrings = [sort_word(word)]

    if len(word) <= 1:
        return substrings

    for pos in range(len(word)):
        short = word[:pos] + word[(pos + 1):]
        short_sorted = sort_word(short)
        these_substrings = get_sorted_substrings(short_sorted)
        substrings.extend(these_substrings)

    substrings_set = set(substrings)
    unique_substrings = list(substrings_set)
    unique_substrings.sort()

    return unique_substrings

response = 'yes'
while response != 'no':
    response = input('Give me a word and I\'ll give you its anagrams.\n')
    sorted_response = sort_word(response)
    
    tic = datetime.datetime.utcnow()
    possible_words = get_sorted_substrings(sorted_response)
    toc = datetime.datetime.utcnow()
    for word in possible_words:
        if word in a_to_z:
            print(score_word(word), a_to_z[word])
    print("Took", (toc - tic).total_seconds(), 'seconds.')
