# tinyurl.com/week116anagrams

def sort_word(word):
    # PURPLE -> ELPPRU
    word = word.upper()
    word_list = list(word)
    word_list.sort()
    return ''.join(word_list)

print(sort_word('purple'))

words_text = open('words.txt', 'r').read()
word_list = words_text.split()

a_to_z = {}
for word in word_list:
    alph = sort_word(word)
    if alph not in a_to_z:
        a_to_z[alph] = []
    a_to_z[alph].append(word)

response = 'yes'
while response != 'no':
    response = input('Give me a word and I\'ll give you its anagrams.\n')
    sorted_response = sort_word(response)
    
    if sorted_response in a_to_z:
        print(a_to_z[sorted_response])
    else:
        print("There are no anagrams of that string.")
