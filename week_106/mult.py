min_num = 1
max_num = 100
html = open('multx.html', 'w')
html.write('<table border="1">\n')
# header row
html.write('    <tr>\n')
html.write('        <th>x</th>\n')
for col in range(min_num, max_num + 1):
    html.write('        <th>%d</th>\n' % col)
html.write('    </tr>\n')

for row in range(min_num, max_num + 1):
    html.write('    <tr>\n')
    html.write('        <th>%d</th>\n' % row)
    for col in range(min_num, max_num + 1):
        prod = row * col
        html.write('        <td>%d</td>\n' % prod)
    html.write('    </tr>\n')

html.write('</table>\n')
