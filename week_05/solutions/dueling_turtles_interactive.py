###############################################################
# TURTLE DUEL
# In this game the computer moves two turtles, red and blue.
# When one turtle pounces on the other, it gets a point, and
# they return to their starting positions.
# First turtle to 5 points wins!
#
# Note: any variable with 'b' refers to the blue turtle, and
#       any variable with 'r' refers to the red  turtle.

###############################################################
# GAME PARAMETERS
# arena size
width   = 600
height  = 110
b_start = (-100, -50) # this is the (x, y) tuple where the blue turtle starts
r_start = ( 100,  50) # this is the (x, y) tuple where the red  turtle starts

# moving parameters
num_moves   = 200
min_angle   = -45
max_angle   =  45
min_length  =  10
max_length  =  40
user_length =  20
speed       =   3 # speed can be 1 to 10 (bigger is faster)

# if the turtles come closer than this number to each other
# then we'll count it as a hit!
collision_distance = 40

# When one turtle reaches this score, the game ends
winning_score = 5

###############################################################
# SET UP THE ARENA AND TURTLES
# Make the first turtle...
import turtle
b = turtle.Pen()

# Use the first turtle to draw the boundaries of the arena
# NOTE: this time we are increasing the width and height by
#       2 * max_length.  This way the turtles won't ever
#       look like they're going out of bounds.
b.pensize(10)
b.up()
b.speed(0) # speed = 0 means that the rectangle is drawn instantly
b.goto(-width/2 - max_length, -height/2 - max_length)
b.down()
b.forward(width + 2 * max_length)
b.left(90)
b.forward(height + 2 * max_length)
b.left(90)
b.forward(width + 2 * max_length)
b.left(90)
b.forward(height + 2 * max_length)
b.left(90)

# Get the blue turtle ready for battle!
b.up()
b.goto(b_start)
b.shape('turtle')
b.color('blue')
b.pensize(10)
b.speed(speed)

# Get the red turtle ready for battle!
r = turtle.Pen()
r.up()
r.shape('turtle')
r.color('red')

r.pensize(10)
r.speed(speed)
r.goto(r_start)
r.left(180)

# We'll need two special packages:
# as usual, we need the random module to pick random movements
# and we need the math module to compute the distance between the turtles
import random
import math

# Initialize each turtle's score.
# Make a Screen object... in this program
# we just use this to display the score in the title bar.
r_score = 0
b_score = 0
s = turtle.Screen()
s.title("Score: Blue %d, Red %d" % (b_score, r_score))

# This flag governs whether or not the player is allowed to move
can_move = True
s.listen()

# A method for computing the distance in pixels between two turtles.
import math
def compute_turtle_distance(t1, t2):
    (t1x, t1y) = t1.position()
    (t2x, t2y) = t2.position()
    return math.sqrt(math.pow(t1x - t2x, 2) + math.pow(t1y - t2y, 2))

# A method for moving the blue turtle in a given direction ('Up', 'Down', 'Right', or 'Left')
def move_blue_turtle(direction):
    global b_score, can_move
    (bx, by) = b.position()
    
    wall_in_the_way = False
    if direction == 'Up':
        new_heading = 90
        if by + user_length > height / 2:
            wall_in_the_way = True
    elif direction == 'Down':
        new_heading = 270
        if by - user_length < -height / 2:
            wall_in_the_way = True
    elif direction == 'Right':
        new_heading = 0
        if bx + user_length > width / 2:
            wall_in_the_way = True
    elif direction == 'Left':
        new_heading = 180
        if bx - user_length < -width / 2:
            wall_in_the_way = True
    
    if can_move and (not wall_in_the_way):
        can_move = False
        b.setheading(new_heading)
        b.forward(user_length)
        if compute_turtle_distance(r, b) < collision_distance:
            b_score = b_score + 1
            s.title("Score: Blue %d, Red %d" % (b_score, r_score))
            b.goto(b_start)
            r.left(720)
            b.goto(b_start)
            r.goto(r_start)
        can_move = True

# Make for methods that call the above method for a given direction
def move_up():
    move_blue_turtle('Up')
def move_down():
    move_blue_turtle('Down')
def move_right():
    move_blue_turtle('Right')
def move_left():
    move_blue_turtle('Left')

# Bind these four methods to the arrow keys.
s.onkey(move_up, 'Up')
s.onkey(move_down, 'Down')
s.onkey(move_right, 'Right')
s.onkey(move_left, 'Left')

###############################################################
# Keep moving each turtle, one at a time
# until one of the two turtles has a big enough score.
while r_score < winning_score and b_score < winning_score:

    ###########################################################
    # RED TURTLE'S TURN
    # make the red turtle move a random direction and distance
    r_angle  = random.uniform(min_angle , max_angle )
    r_length = random.uniform(min_length, max_length)
    r.left(r_angle)
    r.forward(r_length)

    # Get the two turtles' positions
    (bx, by) = b.position()
    (rx, ry) = r.position()
    # Make sure that the blue turtle doesn't go out of bounds!
    if rx > width / 2 or rx < -width / 2 or ry > height / 2 or ry < -height / 2:
        r.left(180)
        r.forward(r_length)

    # Compute the distance between the two turtles,
    # and give the red turtle a point if it hit the blue turtle!
    if compute_turtle_distance(r, b) < collision_distance:
        r_score = r_score + 1
        s.title("Score: Blue %d, Red %d" % (b_score, r_score))
        can_move = False
        b.left(720) # turtles spin when they get hurt!
        b.goto(b_start)
        r.goto(r_start)
        can_move = True

###############################################################
# REPORT THE FINAL RESULT
if b_score > r_score:
    s.title("BLUE WINS!  Score: Blue %d, Red %d" % (b_score, r_score))
else:
    s.title("RED WINS!  Score: Blue %d, Red %d" % (b_score, r_score))

# Set can_move to be true to deactivate motion
can_move = False
#input()
