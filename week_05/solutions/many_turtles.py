# arean size
width   = 201
height  = 201
start_radius = 10
b_start = (-100, 0)
r_start = ( 100, 0)

turtle_count = 20
turtle_colors = ['red', 'orange', 'yellow', 'green', 'blue', 'violet']

# moving parameters
num_moves = 200
min_angle  = -45
max_angle  = 45
min_length = 0
max_length = 20

import turtle

turtle_list = []
for i in range(turtle_count):
    turtle_list.append(turtle.Pen())

t = turtle_list[0]
t.pensize(10)
t.up()
t.goto(-width/2,-height/2)
t.down()
t.forward(width)
t.left(90)
t.forward(height)
t.left(90)
t.forward(width)
t.left(90)
t.forward(height)
t.left(90)
t.up()
t.goto(0,0)

import random

for i in range(len(turtle_list)):
    t = turtle_list[i]
    t.shape('turtle')
    t.pensize(10)
    t.speed(10)
    t.up()
    
    r = random.uniform(0, 1)
    g = random.uniform(0, 1)
    b = random.uniform(0, 1)
    t.color((r, g, b))
#    t.color(turtle_colors[i % len(turtle_colors)])
    t.setheading(360 * i / turtle_count)
    t.back(start_radius)

for i in range(num_moves):
    for t in turtle_list:
        angle  = random.uniform(min_angle , max_angle )
        length = random.uniform(min_length, max_length)
        t.left(angle)
        t.forward(length)
        (x, y) = t.position()
        if x > width / 2 or x < -width / 2 or y > height / 2 or y < -height / 2:
            t.left(180)
            t.forward(length)

input()
