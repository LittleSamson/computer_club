# Make the text of the riddle
# (note the empty lines and spaces to make it stand out)
riddle = """
    Alive without breath,
    As cold as death;
    Never thirsty, ever drinking,
    All in mail never clinking.
"""

# This is the answer.  Shh... it's a secret!
answer = 'fish'

# This is the number of guesses the player is allowed to make
num_guesses = 5

# print the riddle
print("Here's a riddle for you!  You have %d guesses." % num_guesses)
print(riddle)

# keep letting the user guess until they get it right or run out of tries.
for i in range(num_guesses):
    # Get the user's next guess
    print("I'm ready for your guess #%d!  Please use only lowercase letters." % (i + 1))
    guess = input()

    # Check to see if the guess is correct, and act accordingly.
    if guess == answer:
        print('Very well done!')
        break
    else:
        print("I'm sorry; that is not the answer.")

# Print the answer at the end regardless of whether or not they got it.
print('The answer was "%s"' % answer)
