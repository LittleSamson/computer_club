# Make the text of the riddle
# (note the empty lines and spaces to make it stand out)
riddle = """
    I have streets but no pavement,
    I have cities but no buildings,
    I have forests but no trees,
    I have rivers yet no water.
"""

# This is the answer.  Shh... it's a secret!
answer = 'map'

# This is the number of guesses the player is allowed to make
num_guesses = 5

# print the riddle
print("Here's a riddle for you!  You have %d guesses." % num_guesses)
print(riddle)

# keep letting the player guess until they get it right or run out of tries.
# TODO: make a 'for' loop that runs 'num_guesses' times
for index in range(num_guesses):
    # TODO: Ask the player for her next guess: 
    print("I'm ready for your guess #%d!  Please use only lowercase letters." % (index + 1))
    # TODO: Get the player's next guess and set it to the variable 'guess'
    guess = input()

    # TODO: Check to see if the guess is correct, and act accordingly.
    if answer == guess:
        print("You are correct!  Nice job!")
        break
    else:
        print("I'm sorry; that is not correct.")

# TODO: Print the answer at the end regardless of whether or not they got it.
print('The answer was "%s"' % answer)
