# Make the text of the riddle
# (note the empty lines and spaces to make it stand out)
riddle = """
    I have streets but no pavement,
    I have cities but no buildings,
    I have forests but no trees,
    I have rivers yet no water.
"""

# This is the answer.  Shh... it's a secret!
answer = 'map'

# This is the number of guesses the player is allowed to make
num_guesses = 5

# print the riddle
print("Here's a riddle for you!  You have %d guesses." % num_guesses)
print(riddle)

# keep letting the player guess until they get it right or run out of tries.
# TODO: make a 'for' loop that runs 'num_guesses' times
    # TODO: Ask the player for her next guess: 
    # e.g., "I'm ready for your guess #1!  Please use only lowercase letters."
    # TODO: Get the player's next guess and set it to the variable 'guess'

    # TODO: Check to see if the guess is correct, and act accordingly.

# TODO: Print the answer at the end regardless of whether or not they got it.
# e.g., 'The answer was "map"'
