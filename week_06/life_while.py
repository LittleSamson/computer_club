age = 0
while age < 10:
    print("You are now %d years old!" % age)
    
    if age == 5:
        age = age + 2
        print("You just skipped 2 years!  Now you're %d years old!" % age)
    age = age + 1
