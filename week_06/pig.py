# This function prints the player's current score
def print_score(points):
    print("You have %d points!" % points)

# This function rolls a 6-sided die, prints the result, and returns it
def roll_the_die():
    import random
    random_roll = random.randint(1, 6)
    print("You rolled a %d!." % random_roll)
    return random_roll

# Start off with a score of zero
# And the die not equal to 1 (to get the while loop started)
score = 0
this_roll = 0

# Keep giving the user a chance to roll again until she rolls a 1 (or quits early)
while this_roll != 1:
    # Print the score so far, and ask the player whether or not she wants 
    print_score(score)
    print("Type 'q' if you want to quit.")
    quit = input()
    if quit == 'q':
        break

    # roll the die again
    this_roll = roll_the_die()

    # reset the score if a 1 is rolled; otherwise, add the value of the die.
    if this_roll == 1:
        score = 0
    else:
        score = score + this_roll

# Report the final score.
print_score(score)
print("Thanks for playing!")
