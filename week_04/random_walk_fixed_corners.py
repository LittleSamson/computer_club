# Values that define how the program runs
num_lines  = 500
max_length =  20
min_angle  = -45
max_angle  =  45

# import the turtle package and initialize a Pen object
import turtle
t = turtle.Pen()
t.shape('turtle')
t.pensize(10)

# draw some boundaries
width = 100
height = 100
t.color('black')
t.up()
t.forward(width / 2)
t.down()
t.left(90)
t.forward(height / 2) # top of right border
t.left(90)
t.forward(width) # top border
t.left(90)
t.forward(height) # left border
t.left(90)
t.forward(width) # bottom border
t.left(90)
t.forward(height / 2) # bottom of right border
t.left(90)
t.up()
t.forward(width / 2)
t.left(180)
t.down()

# import the package for generating random numbers
import random

# a list of colors in the rainbow!
rainbow_list = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
# Repeat the following block a certain number of times...
for i in range(num_lines):
    # pick a random angle and a random length
    angle  = random.uniform(min_angle, max_angle)
    length = random.uniform(        0, max_length)

#    # pick a random color (red, green, and blue values between 0 and 1)
#    red   = random.uniform(0, 1)
#    green = random.uniform(0, 1)
#    blue  = random.uniform(0, 1)
#    rgb = (red, green, blue)

    # Pick the next color of the rainbow!
    rgb = rainbow_list[i % len(rainbow_list)]
    
    # Set the new color 
    t.color(rgb)

    # Turn and draw!
    t.left(angle)
    t.forward(length)
    
    # Get the current position:
    # x: how far right (positive) or left (negative) you are from the center
    # y: how far up    (positive) or down (negative) you are from the center
    (x, y) = t.position()

    # if you go out of bounds, turn around and go back the way you came!
    if x > width / 2:
        t.left(180)
        t.forward(length)
    elif x < -width / 2:
        t.left(180)
        t.forward(length)
    elif y > width / 2:
        t.left(180)
        t.forward(length)
    elif y < -width / 2:
        t.left(180)
        t.forward(length)

#    if x > width / 2 or x < -width / 2 or y > height / 2 or y < -height / 2:
#        t.left(180)
#        t.forward(length)

#input('Press ENTER to exit.')

