# Set up the minimum and maximum number, and pick a random number between them (inclusive)
import random

min_num =   1
max_num = 100
num = random.randint(min_num, max_num)

# introductory text
print("I'm thinking of a number between %d and %d." % (min_num, max_num))

# Keep prompting the user for guesses until they get it right
guess = min_num - 1
while guess != num:
    # Get the user's next guess
    guess_str = input("What is your guess? ")
    
    # TODO: Convert the guessed string into an integer
    
    # TODO: print various things based on whether
    # the number is too big, too small, or just right

