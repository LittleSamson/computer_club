##################################################
# SETUP 
# TODO: make a list of possible choices
choice_list = ['rock', 'paper', 'scissors']

##################################################
# PLAYER CHOICE
# Greet the player and ask them to choose
print("Let's play rock, paper, scissors!")
print("When you're ready, pick 'rock', 'paper', or 'scissors'!")
# TODO: get input from the user
player_choice = input()

# complain and quit if the player chose an invalid option.
if player_choice not in choice_list:
    import sys
    print("Hmm... I don't know what you mean.")
    sys.exit(0)

##################################################
# COMPUTER CHOICE
# TODO: pick a random number (0, 1, or 2)
import random
random_integer = random.randint(0, 2)

# Convert the number 0, 1, or 2 to the choice
computer_choice = choice_list[random_integer]

##################################################
# FIGURE OUT WHO WON!
print("You chose %s, eh?  Well, I chose %s!" % (player_choice, computer_choice))

# TODO: Write if/elif/else conditions for each possibility, and print a suitable message
if player_choice == computer_choice:
    print("It's a tie!")
elif player_choice == 'rock' and computer_choice == 'scissors':
    print("You win!")
elif player_choice == 'paper' and computer_choice == 'rock':
    print("You win!")
elif player_choice == 'scissors' and computer_choice == 'paper':
    print("You win!")
else:
    print("You lose.") 

# Be a good sport no matter what!
print("Thanks for playing!")
