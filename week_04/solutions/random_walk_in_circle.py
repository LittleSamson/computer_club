import math
# Values that define how the program runs
num_lines  = 200
max_length =  20
min_angle  = -45
max_angle  =  45

# import the turtle package and initialize a Pen object
import turtle
t = turtle.Pen()
t.shape('turtle')
t.pensize(10)

# draw some boundaries
radius = 100.0
t.color('black')
t.up()
t.right(90)
t.goto(0, -radius) # go to the bottom of the circle
t.left(90)
t.down()
t.circle(radius)
t.up()
t.goto(0, 0) # journey to the center of the earth
t.down()

# import the package for generating random numbers
import random

colorList = ['red', 'orange', 'yellow', 'green', 'blue', 'violet']
# Repeat the following block a certain number of times...
for i in range(num_lines):
    # pick a random angle and a random length
    angle  = random.uniform(min_angle, max_angle)
    length = random.uniform(        0, max_length)

    # pick a random color (red, green, and blue values between 0 and 1)
    red   = random.uniform(0, 1)
    green = random.uniform(0, 1)
    blue  = random.uniform(0, 1)
    rgb = (red, green, blue)
    rgb = colorList[i % len(colorList)]
    
    # Set the new color 
    t.color(rgb)

    # Turn and draw!
    t.left(angle)
    t.forward(length)
    
    # Get the current position:
    # x: how far right (positive) or left (negative) you are from the center
    # y: how far up    (positive) or down (negative) you are from the center
    (x, y) = t.position()

    # if you go out of bounds, turn around and go back the way you came!
    # enforce the boundary of the circle here!
    if (x * x + y * y) >= radius * radius:
        t.left(180)
        t.forward(length)

#input('Press ENTER to exit.')

