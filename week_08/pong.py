import os, time, turtle

#########################################
# Parameters

# screen
winning_score =  5
bgcolor = 'black'
color   = 'green'

# rectangle boundary
middle  =   0
width   = 600
height  = 413
left    = -width  / 2
right   =  width  / 2
bottom  = -height / 2
top     =  height / 2
padding = 18

# paddle parameters
paddle_size   = 1
paddle_len    = 5 # it's not clear what these mean
paddle_wid    = 1 # it't not clear what these mean
paddle_speed  = 10
paddle_step   = 20
paddle_height = 90 # determined empirically to match visible height
collision_height = paddle_height + 25 # bigger than paddle_height: grace!

# ball parameters
ball_size  =  5
ball_speed = 10
ball_step  =  5

#########################################
# Game setup

# Screen
s = turtle.Screen()
s.bgcolor(bgcolor)
score_1 = 0
score_2 = 0
s.title("Player 1: %d    Player 2: %d" % (score_1, score_2))
s.listen()

# draw the boundaries
t = turtle.Pen()
t.color(color)
t.pensize(20)
t.speed(0)
t.shape('square')
t.up()
t.setpos(left, bottom - padding)
t.down()
t.forward(width)
t.left(90)
t.up()
t.forward(height + 2 * padding)
t.down()
t.left(90)
t.forward(width)
t.left(90)
t.up()
t.forward(height + 2 * padding)
t.down()
t.left(90)
t.up()
t.forward(1000)

# Make the players' paddles
# player 1
p1 = turtle.Pen()
p1.color(color)
p1.speed(paddle_speed)
p1.up()
p1.goto(left - padding, middle)
p1.left(90)
p1.shape('square')
p1.pensize(paddle_size)
p1.turtlesize(paddle_wid, paddle_len)

# player 2
p2 = turtle.Pen()
p2.color(color)
p2.speed(paddle_speed)
p2.up()
p2.goto(right + padding, middle)
p2.left(90)
p2.shape('square')
p2.pensize(paddle_size)
p2.turtlesize(paddle_wid, paddle_len)

# Make the ball
ball = turtle.Pen()
ball.color(color)
ball.pensize(ball_size)
ball.up()
ball.shape('circle')
ball.speed(ball_speed)
ball.left(45)

# Paddle movement functions
def move_turtle(t, direction):
    (x, y) = t.position()
    if direction == 'Up':
        new_y = min(y + paddle_step, top    - paddle_height / 2)
        t.goto(x, new_y)
    elif direction == 'Down':
        new_y = max(y - paddle_step, bottom + paddle_height / 2)
        t.goto(x, new_y)

def move_1_up():
    move_turtle(p1, 'Up')
def move_1_down():
    move_turtle(p1, 'Down')
def move_2_up():
    move_turtle(p2, 'Up')
def move_2_down():
    move_turtle(p2, 'Down')

def exit_game():
    os.sys.exit(0)

s.onkey(move_1_up  , 'a')
s.onkey(move_1_down, 'z')
s.onkey(move_2_up  , 'Up')
s.onkey(move_2_down, 'Down')
s.onkey(exit_game, 'Escape')

#########################################
# play the game!

# Keep moving the ball until the winning score has been reached
while max(score_1, score_2) < winning_score:
    # Get the ball's position and heading (angle)
    (x, y) = ball.position()
    theta  = ball.heading()
    
    # Reflect off the top wall if you hit it
    if (y >= (top - ball_step)) and (theta < 180):
        ball.setheading(180 + (180 - theta))

    # Reflect off the bottom wall if you hit it
    if (y <= (bottom + ball_step)) and (theta > 180):
        ball.setheading(360 - theta)

    # If you are at the left boundary...
    if (x <= (left + ball_step)) and ((theta > 90) and (theta < 270)):
        # Reflect off the player 1 paddle if you hit it.
        (p1x, p1y) = p1.position()
        if y >= (p1y - collision_height / 2) and y <= (p1y + collision_height / 2):
            ball.setheading(180-theta)

        # If player 1 missed, pause the game, give player 2 a point, etc.
        else:
            ball.forward(ball_step)
            score_2 += 1
            s.title("Player 1: %d    Player 2: %d" % (score_1, score_2))
            ball.color('red')
            time.sleep(2)
            ball.color(color)
            ball.goto(middle, middle)
            ball.left(180)

    # If you are at the right boundary...
    if (x >= (right - ball_step)) and ((theta < 90) or (theta > 270)):
        # Reflect off the player 2 paddle if you hit it.
        (p2x, p2y) = p2.position()
        if y >= (p2y - collision_height / 2) and y <= (p2y + collision_height / 2):
            ball.setheading(180-theta)

        # If player 2 missed, pause the game, give player 1 a point, etc.
        else:
            ball.forward(ball_step)
            score_1 += 1
            s.title("Player 1: %d    Player 2: %d" % (score_1, score_2))
            ball.color('red')
            time.sleep(2)
            ball.color(color)
            ball.goto(middle, middle)
            ball.left(180)

    # Move the ball forward each time in the loop
    ball.forward(ball_step)

# Report the final score
if score_1 > score_2:
    s.title("Player 1: %d    Player 2: %d ... PLAYER 1 WINS!" % (score_1, score_2))
else:
    s.title("Player 1: %d    Player 2: %d ... PLAYER 2 WINS!" % (score_1, score_2))

# Players can continue to move their paddles until input is entered into the shell.
t.up()
t.goto(middle, top + 50)
t.write('Press Escape to quit.', align='center', font=("Arial", 20, "normal"))
t.goto(2 * width, 2 * height)
input()

