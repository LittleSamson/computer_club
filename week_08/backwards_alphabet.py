# Get input message from the user
message_in = input('What is your message?')

# Convert the message to upper case so that there are fewer things to check
message_in = message_in.upper()

# build a dict mapping input to output letters
# (either direction)
import string
forward = string.ascii_uppercase
reverse = forward[::-1]

letter_map = {}
for i in range(len(forward)):
    letter_map[forward[i]] = reverse[i]
	
# Put together a string that translates each letter from response
message_out = ''
for y in message_in:
	if y in letter_map:
		message_out += letter_map[y]
	else:
		message_out += y

# Print the output message
print(message_out)
		
