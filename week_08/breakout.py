import os, time, turtle

#########################################
# Parameters

# lives/score
lives = 3
score = 0

# room parameters
bgcolor        = 'black'
wall_color     = 'white'
middle         =   0
start          = -20
room_width     = 650
room_height    = 500
wall_thickness =  18

# paddle parameters
paddle_color           = 'white'
paddle_size            = 1
paddle_speed           = 10
paddle_step            = 20
paddle_height          = 90 # determined empirically to match visible height
paddle_altitude        = -18
collision_height       = paddle_height + 25 # bigger than paddle_height: grace!
paddle_visible_width   = 5 # it's not clear what these mean
paddle_visible_height  = 1 # it't not clear what these mean

# brick parameters
brick_colors = ['red', 'orange', 'yellow', 'green', 'blue', 'cyan', 'violet']
row_count    = 7
col_count    = 6
brick_size   = 2
brick_width  = 90
brick_height = 10
brick_padding = 18
brick_visible_width  = 5
brick_visible_height = 1

# ball parameters
ball_color = 'white'
ball_size  =  5
ball_speed = 10
ball_step  =  5

#########################################
# Game setup

# Screen
s = turtle.Screen()
s.bgcolor(bgcolor)
s.title("Lives: %d        Score: %d" % (lives, score))
s.listen()

#################################
# Room class
class Room(turtle.Pen):
    def __init__(self, width, height, wall_thickness, color):
        # Do the usual initialization for a turtle.Pen object
        super(Room, self).__init__()

        # Save the parameters provided by the caller
        self.width          = width
        self.height         = height
        self.wall_thickness = wall_thickness

        # The locations of the room walls are at half the room width and height
        self.wall_left  = -width  / 2
        self.wall_right =  width  / 2
        self.floor      = -height / 2
        self.ceiling    =  height / 2

        # Cursor characteristics
        self.color(color)
        self.pensize(wall_thickness)
        self.speed(0)
        self.shape('square')

        # Draw the boundaries of the room
        self.up()

        # left wall
        self.setpos(self.wall_left - wall_thickness / 2, self.floor)
        self.left(90)
        self.down()
        self.forward(self.height + wall_thickness / 2)

        # top wall
        self.right(90)
        self.forward(self.width + wall_thickness)

        # right wall
        self.right(90)
        self.forward(self.height + wall_thickness / 2)
        self.left(90)

        # Make the cursor disappear!
        self.up()
        self.forward(1000)

    def check_wall_and_bounce(self, ball):
        
        # Determine whether or not the wall has been hit by the ball...
        (x, y) = ball.position()
        theta  = ball.heading()
        hit_wall = False

        # Reflect off the top wall if you hit it
        if (y >= (self.ceiling - ball_step)) and (theta < 180):
            ball.setheading(180 + (180 - theta))
            hit_wall = True
    
        # Reflect off the left wall if you hit it
        if (x <= (self.wall_left + ball_step)) and ((theta > 90) and (theta < 270)):
            ball.setheading(180-theta)
            hit_wall = True
    
        # Reflect off the right wall if you hit it
        if (x >= (self.wall_right - ball_step)) and ((theta < 90) or (theta > 270)):
            ball.setheading(180-theta)
            hit_wall = True

        return hit_wall


#################################
# Brick class
class Brick(turtle.Pen):
    def __init__(self, center_x, center_y, width, height, color, points):
        # Do the usual initialization for a turtle.Pen object
        super(Brick, self).__init__()

        # Save the parameters provided by the caller
        self.center_x = center_x
        self.center_y = center_y
        self.width    = width
        self.height   = height
        self.points   = points

        # Make a brick of the right size, shape, color at the desired position
        self.up()
        self.color(color)
        self.speed(0)
        self.goto(center_x, center_y)
        self.right(90)
        self.shape('square')
        self.turtlesize(brick_visible_width, brick_visible_height)
        self.pensize(brick_size)

    def get_points(self):
        return self.points

    def hit(self, ball):
        # Determine whether or not the brick has been hit by the ball...

        # Determine the location of the ball, and the boundaries of the brick
        (x, y) = ball.position()
        (brick_x, brick_y) = self.position()
        brick_bottom = brick_y - brick_height / 2 - 12
        brick_top    = brick_y + brick_height / 2 + 12
        brick_left   = brick_x - brick_width  / 2 - 12
        brick_right  = brick_x + brick_width  / 2 + 12

        # If the we believe that the ball has struck the brick, reverse its y-direction
        if (x >= brick_left) and (x <= brick_right) and (y >= brick_bottom) and (y <= brick_top):
            theta = ball.heading()
            if theta < 90:
                ball.setheading(-theta)
            elif theta < 180:
                ball.setheading(360 - theta)
            elif theta < 270:
                ball.setheading(360 - theta)
            else:
                ball.setheading(-theta)
            self.disappear()
            return True

        return False
    
    def disappear(self,):
        self.goto(1000, 1000)

#################################
# Ball class
class Ball(turtle.Pen):
    def __init__(self, start_x, start_y, size, orig_color, orig_speed, step):
        # Do the usual initialization for a turtle.Pen object
        super(Ball, self).__init__()

        # Save the parameters provided by the caller
        self.start_x    = start_x
        self.start_y    = start_y
        self.size       = size
        self.orig_color = orig_color
        self.orig_speed = orig_speed
        self.step       = step

        # Lift up the pen
        self.up()
        self.pensize(size)
        self.shape('circle')
        self.speed(orig_speed)

        # Put the ball in its starting position
        self.reset()

    def reset(self,):
        # Reset the ball to its starting position
        self.color(self.orig_color)
        self.goto(self.start_x, self.start_y)
        self.setheading(-45)


# Set up the room!
room = Room(room_width, room_height, wall_thickness, wall_color)

# Make the paddle
p = turtle.Pen()
p.color(paddle_color)
p.speed(paddle_speed)
p.up()
p.goto(middle, -room_height / 2 - 20)
p.shape('square')
p.pensize(paddle_size)
p.left(90)
p.turtlesize(paddle_visible_width, paddle_visible_height)

# Make the ball
ball = Ball(0, start, ball_size, ball_color, ball_speed, ball_step)

# Set up a bunch of bricks
bricks = []
for row in range(row_count):
    for col in range(col_count):
        x = -room_width / 2 + wall_thickness / 2 + brick_width / 2 + col * (brick_width + brick_padding)
        y = middle + row * (brick_height + brick_padding)
        brick = Brick(x, y, brick_width, brick_height, brick_colors[row % len(brick_colors)], 10 * (row + 1))
        bricks.append(brick)

# Paddle movement functions
def move_paddle(t, direction):
    (x, y) = t.position()
    if direction == 'Left':
        new_x = max(x - paddle_step, -room_width/2 + paddle_height / 2)
        t.goto(new_x, y)
    elif direction == 'Right':
        new_x = min(x + paddle_step, room_width / 2 - paddle_height / 2)
        t.goto(new_x, y)

def move_left():
    move_paddle(p, 'Left')
def move_right():
    move_paddle(p, 'Right')

def exit_game():
    os.sys.exit(0)

s.onkey(move_left , 'Left')
s.onkey(move_right, 'Right')
s.onkey(exit_game, 'Escape')

#########################################
# play the game!

# Keep moving the ball until the winning score has been reached
while (lives > 0) and bricks:
    # Get the ball's position and heading (angle)
    (x, y) = ball.position()
    theta  = ball.heading()

    room.check_wall_and_bounce(ball)
    
    # If you are at the bottom boundary...
    if (y <= (-room_height/2 + ball_step)) and (theta > 180):
        # Reflect off the paddle if you hit it.
        (px, py) = p.position()
        if x >= (px - collision_height / 2) and x <= (px + collision_height / 2):
            ball.setheading(180 + (180 - theta))
#            if x < (px - 0.4 * collision_height):
#                ball.setheading(-155)
#            elif x < (px - 0.25 * collision_height):
#                ball.setheading(-145)
#            elif x < px + 0.25 * collision_height:
#                ball.setheading(180 + (180 - theta))
#            elif x < (px - 0.4 * collision_height):
#                ball.setheading(35)
#            else:
#                ball.setheading(25)


        # Otherwise, the player loses a life
        else:
            ball.forward(ball_step)
            lives -= 1
            s.title("Lives: %d        Score: %d" % (lives, score))
            ball.color('red')
            time.sleep(2)
            ball.reset()

    # Check the bricks...
    for i, brick in enumerate(bricks):
        if brick.hit(ball):
            score += brick.get_points()
            s.title("Lives: %d        Score: %d" % (lives, score))
            del bricks[i]
            break

    # Move the ball forward each time in the loop
    ball.forward(ball_step)


# Players can continue to move their paddles until input is entered into the shell.
t = turtle.Pen()
t.color(wall_color)
t.up()
t.speed(0)
t.goto(middle, room_height/2 + 50)
t.write('Press Escape to quit.', align='center', font=("Arial", 20, "normal"))
t.goto(2 * room_width, 2 * room_height)

