from tkinter import *

############################################################
# A Room class!
class Room(object):
    def __init__(self, num, options, description, coord, item_list=[], barrier_list=[]):
        # Save off the inputs:
        # num         : the number of this room
        # options     : a dict mapping directions (N, S, E, W) to room numbers
        # description : a description of the room to be printed upon entry
        # coord       : a tuple of (x, y) coordinates from measured from the NW corner of the canvas)
        # item_list   : a list of Item objects that can be found in this room
        # barrier_list: a list of Barrier objects in this room
        self.num          = num
        self.options      = options
        self.description  = description
        self.coord        = coord
        self.item_list    = item_list
        self.barrier_list = barrier_list

        # Make sure that item and barrier lists are actually lists
        if not isinstance(self.item_list, list):
            self.item_list = [self.item_list]
        if not isinstance(self.barrier_list, list):
            self.barrier_list = [self.barrier_list]
    
    def enter(self, inventory):
        # Enter the room and describe it
        print(self.description)
        
        # Describe each barrier in the room (if there are any)
        for barrier in self.barrier_list:
            desc = barrier.get_description()
            if desc:
                print(desc)

        # Describe each item in the room (if there are any)
        for item in self.item_list:
            if item.description:
                print(item.description)
        
        # Tell the player the options and get her choice
        response = None
        while response not in self.options:
            exits = list(self.options)
            exits.sort()
            print('Obvious exits are', exits)
            print("Which way would you like to go? (or 'GET ITEM' or 'USE ITEM')")
            response = input()

            # see if the player wants to get an item
            self.get_item(response, inventory)

            # see if the player wants to use an item
            self.use_item(response, inventory)

        # Report which way she went
        directions = {'N': 'north', 'S': 'south', 'E': 'east', 'W': 'west'}
        if response in directions:
            print('You went %s.' % directions[response])
        print()

        # Return the room number of the response
        return self.options[response]
     
    def get_item(self, player_response, inventory):
        """Given a player response, see if they want to get an item.
        If that item is in the room, put it in the inventory and remove it from the room.
        Otherwise, tell them that the item is not in the room."""

        if player_response.upper().startswith('GET'):
            # remove the word 'GET' and strip off any spaces
            desired_item = player_response[3:].strip()

            # If there's an item by this name in the room, then get it!
            for item_index, item in enumerate(self.item_list):
                if desired_item == item.name:
                    print('You got the %s,' % item.name)
                    # add it to the invoentory
                    inventory.append(item)
                    # remove it from the room
                    del self.item_list[item_index]
                    return

            # otherwise, this item isn't in the room,.
            else:
                print('Sorry; you cannot get the %s.' % desired_item)
     
    def use_item(self, player_response, inventory):
        """Given a player response, see if they want to use an item.
        If that item is in the inventory and usable in this room,
        activate its options for this room and remove it from the inventory
        Otherwise, print an error message"""

        if player_response.upper().startswith('USE'):
            # remove the word 'USE' and strip off any spaces
            desired_item = player_response[3:].strip()

            for item in inventory:
                if item.name == desired_item:
                    # look for a barrier in this room on which this item can be used.
                    for barrier in self.barrier_list:
                        unlocked_options = barrier.remove(item)
                        if unlocked_options is not None:
                            self.options.update(unlocked_options)
                            return

                    # if you made it out of the barrier for loop, then the item could not be used.
                    print("Sorry; you can't use the %s here." % desired_item)
                    return

            # if you made it out of the item for loop, then the player doesn't have this item...
            print("Sorry; you don't have the %s." % desired_item)
            return

    def get_coordinates(self,):
        """Return this room's tuple of (x, y) coordinates,
        measured from the NW corner of the canvas"""
        return self.coord


############################################################
# A Trap is special kind of room with no choices: you just die.
class Trap(Room):
    def enter(self, inventory):
        print(self.description)
        print('Game Over.')

############################################################
# An Exit is a special kind of room with no choices: you win!
class Exit(Room):
    def enter(self, inventory):
        print(self.description)
        print('Congratulations!')
        return None


############################################################
# An Item class for items that can be gotten and used
class Item(object):
    def __init__(self, name, description):
        """input parameters
        - name        - the name of the item
        - description - the initial description of the item in the room in which it is found
"""
        self.name        = name
        self.description = description

############################################################
# An class for barriers that must be overcome
class Barrier(object):
    def __init__(self, before_desc, after_desc, item_options):
        """input parameters:
        - before_desc  - the original description of the barrier
        - after_desc   - the description of the barrier after it has been removed
        - item_options - a dict mapping an item name that will overcome the barrier
                         to a dict of new options available in the room
"""
        self.before_desc  = before_desc
        self.after_desc   = after_desc
        self.item_options = item_options
    
        # this attribute indicates whether or not the barrier has been removed
        self.removed = False

    def get_description(self, ):
        """Return the after or before description depending on whether or
        not the barrier has been removed, respectively"""
        if self.removed:
            return self.after_desc
        else:
            return self.before_desc

    def remove(self, item):
        """Attempt to use the given item to remove this barrier
        Return the dict of new options if this is possible, and None otherwise."""
        # The barrier must not already have been removed
        if not self.removed:
            if item.name in self.item_options:
                self.removed = True
                print(self.after_desc)
                return self.item_options[item.name]

        # Return None if the item could not remove the barrier
        return None
            

############################################################
# A dungeon class!
class Dungeon(object):
    def __init__(self,):
        # The name and author of the game
        self.title = 'Escape the Dungeon'
        self.author = 'David Hansen'
        
        self.inventory = []

        # Make any items that are necessary
        key = Item(
            'KEY',
            'There is a KEY under the ham.',
        )

        sword = Item(
            'SWORD',
            'Dennis is holding a SWORD out to you.'
        )

        # Make any barriers that are necessary
        door_to_9 = Barrier(
            'There is a locked door to the south.',
            'The door has been unlocked.',
            {'KEY': {'S': 9}},
        )

        dragon = Barrier(
            'There is a fearsome dragon to the east!',
            'The dragon has been slain.',
            {'SWORD': {'E': 14}},
        )

        # Make a list of rooms
        self.rooms = []

        # add each room to the list
        # It's up to you to make sure that your dungeon is complete!
        # self.add_room()
        # self.add_trap()
        # etc.
        self.add_trap( 
            0,
            "It's a trap!  The reindeer tramples you to death.",
            (157, 15),
        )

        self.add_trap(
            1,
            'A bear eats you!',
            (22, 66),
        )

        self.add_entrance(
            2,
            {'W': 1, 'S': 6, 'E': 3},
            "There's a pumpkin in the center of the room.",
            (86, 66),
        )

        self.add_room(
            3,
            {'N': 0, 'W': 2, 'E': 4, 'S': 7},
            "Santa Claus is here! \"Go North! Ho, ho, ho!\" he says.",
            (155, 64),
        )

        self.add_room(
            4,
            {'W': 3, 'E': 5},
            "There is a candle in the center of the room.",
            (270, 64),
            barrier_list=door_to_9
        )

        self.add_trap(
            5,
            "You are crushed by an avalanche of potatoes!",
            (354, 60),
        )

        self.add_trap(
            6,
            "The Death Star destroys you!",
            (85, 127),
        )

        self.add_room(
            7,
            {'W': 6, 'S': 11, 'N': 3},
            'There is a stray cat. "Meow," it says.',
            (154, 126),
        )

        self.add_trap(
            8,
            "You fall into a bottomless pit!",
            (204, 126),
        )

        self.add_room(
            9,
            {'N': 4, 'S': 13, 'E': 10, 'W': 8},
            'There is a delicious canteloupe in the center of the room!',
            (271, 125),
        )

        self.add_room(
            10,
            {'W': 9},
            'There is a venomous cobra in this room!  Fortunately, it is kind.',
            (352, 124),
        )

        self.add_room(
            11,
            {'N': 7},
            'This room contains a leftover ham.',
            (155, 186),
            item_list=key
        )

        self.add_room(
            12,
            {'E': 13},
            'Dennis is here!',
            (207, 185),
            item_list=sword
        )

        self.add_room(
            13,
            {'N': 9, 'S': 15, 'W': 12},
            'The watchdog is here. "You\'re running out of time!" he says.',
            (270, 180),
            barrier_list = dragon
        )

        self.add_exit(
            14,
            'The bright sunlight pierces your eyes as you step out of the cave.  You have escaped!',
            (350, 180),
        )

        self.add_trap(
            15,
            'This room has spikes. Sharp spikes.',
            (271, 250),
        )

        # Set up the canvas with a background image... a map of the dungeon! 
        self.tk = Tk()
        self.canvas = Canvas(self.tk, width=400, height=400)
        self.canvas.pack()
        
        image_name = 'dungeon_escape.gif'
        self.image = PhotoImage(file=image_name)
        self.canvas.create_image(0, 0, anchor=NW, image=self.image)

        # Create the player!
        entrance = self.get_room(self.entrance_num)
        self.coord = entrance.get_coordinates()
        x1 = self.coord[0]
        y1 = self.coord[1]
        x2 = x1 + 10
        y2 = y1 + 10
        self.square = self.canvas.create_rectangle(x1, y1, x2, y2, fill='red', outline='black')
        

    # This helper function adds a regular Room to the list of rooms
    def add_room(self, num, options, description, coord, item_list=[], barrier_list=[]):
        self.rooms.append(Room(num, options, description, coord, item_list, barrier_list))

    # This helper function adds a Trap to the list of rooms
    # No options are necessary since there is no escape!
    def add_trap(self, num, description, coord):
        self.rooms.append(Trap(num, [], description, coord))

    # This helper function adds an Exit to the list of rooms
    # No options are necessary since you win!
    def add_exit(self, num, description, coord):
        self.rooms.append(Exit(num, [], description, coord))

    # This helper function adds a regular Room to the list of rooms
    # but marks its number as the room in which the game starts
    def add_entrance(self, num, options, description, coord, item_list=[], barrier_list=[]):
        self.add_room(num, options, description, coord, item_list, barrier_list)
        self.entrance_num = num

    def get_room(self, room_number):
        """find the room corresponding to the given number"""
        for room in self.rooms:
            if room.num == room_number:
                return room

        raise Exception('No room could be found with number %d' % room_number)

    # This function controls the gameplay
    def explore(self,):
        # Print some text to start the game
        print()
        print("    %s" % self.title)
        print("        by %s" % self.author)
        print()
        print("Press Enter to start exploring.")
        input()
        
        # Start at the entrance
        room_number = self.entrance_num

        # Keep going through rooms until you get to the exit or a trap.
        while room_number is not None:
#            # Clear the screen for the next room
#            print('\n' * 100)

            # Find the current room in the list using the latest room number
            current_room = self.get_room(room_number)

            (old_x, old_y) = self.coord
            (new_x, new_y) = current_room.get_coordinates()
            self.canvas.move(self.square, new_x - old_x, new_y - old_y)
            self.coord = (new_x, new_y)

            # enter this room and the number of the player's next choice
            room_number = current_room.enter(self.inventory)

############################################################
# Game play!

# Make a dungeon
dungeon = Dungeon()

# Explore it.
dungeon.explore()

input() #
