from tkinter import *
import random
tk = Tk()
canvas = Canvas(tk, width=400, height=400)
canvas.pack()

def random_rectangle(width, height):
    x1 = random.randrange(width)
    y1 = random.randrange(height)
    x2 = x1 + random.randrange(width)
    y2 = y1 + random.randrange(height)

    color = random.randint(0, 0xffffff)
    color_str = '#%06x' % color

    canvas.create_rectangle(x1, y1, x2, y2, outline=color_str)

for x in range(0, 100):
    random_rectangle(400, 400)

input()
